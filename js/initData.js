//Constantes de datos
const dataSectores = 'https://api.elconfidencial.com/service/resource/dashboard-economia-sectores/';
const dataEmpleo = 'https://api.elconfidencial.com/service/resource/dashboard-economia-empleo/';
const dataDemanda = 'https://api.elconfidencial.com/service/resource/dashboard-economia-demanda/';

const dataSectoresLocal = './local-data/sectores.tsv';
const dataEmpleoLocal = './local-data/empleo.tsv';
const dataDemandaLocal = './local-data/demanda.tsv';

let slideWidth, slideHeight, areaWidth, areaHeight;

//Indicadores relativos a cada área de análisis (En la variable aparece, primero, el área de análisis; después, el indicador)

let sectoresCambioAfiliacion = [], sectoresValorAnadido = [], sectoresLlegadaViajeros = [];
let empleoAfiliacionProvincias = [], empleoAfiliacionHistorico = [], empleoTasaParoHistorica = [], empleoCaidaEmpleo = [], empleoTasaParoEuropa = [], empleoNumeroParados = [];
let demandaMinoristaHistorica = [], demandaMinoristaSectorial = [], demandaMinoristaCCAA = [], demandaMinoristaEurozona = [];

if(env == 'local'){
    getData(dataSectoresLocal, 'dataSectores');
    getData(dataEmpleoLocal, 'dataEmpleo');
    getData(dataDemandaLocal, 'dataDemanda');
} else {
    getData(dataSectores, 'dataSectores');
    getData(dataEmpleo, 'dataEmpleo');
    getData(dataDemanda, 'dataDemanda');
}


///// 
///// Una vez tenemos todos los datos en variables, podemos empezar a pintar carruseles y gráficos | Comprobar si, en efecto, ya están los datos cargados 
/////

let focusSlide = 0;
let isSliderVisible = false;
const areaOrder = ['produccion', 'expectativas', 'cuentas_publicas', 'empleo', 'demanda', 'sectores'];

const sectores = ['sectoresCambioAfiliacion', 'sectoresValorAnadido', 'sectoresLlegadaViajeros'];

const empleo = ['empleoAfiliacionProvincias', 'empleoAfiliacionHistorico', 'empleoTasaParoHistorica', 'empleoCaidaEmpleo', 'empleoTasaParoEuropa', 'empleoNumeroParados']; 
const demanda = ['demandaMinoristaHistorica', 'demandaMinoristaSectorial', 'demandaMinoristaCCAA', 'demandaMinoristaEurozona'];

const mapaAutonomias = 'https://www.ecestaticos.com/file/2d25f5ae64e34b03073ee5c2654863c0/1595321804-autonomias_ceuta_melilla_mapshaper.json';

let activeArea = '';
let previousArea = '';

function createArea(menuOption){
    //Primero borramos todos los hijos
    //let area = document.getElementsByClassName('area')[0];
    let area = document.getElementById('areaCards');
    while(area.firstChild){
        area.removeChild(area.firstChild);
    }

    //Añadimos clase modificadora en div de área
    if ( activeArea!='' ) {
        document.getElementsByClassName("area")[0].classList.remove("area--"+activeArea);
    }
    document.getElementsByClassName("area")[0].classList.add("area--"+menuOption);
    activeArea = menuOption

    //CAMBIAR CUANDO TENGAMOS CLARO QUE IRÁ EN LA PORTADA DE CADA SECCIÓN
    let arraySlider = menuOption == 'produccion' ? produccion 
        : menuOption == 'empleo' ? empleo 
        : menuOption == 'demanda' ? demanda 
        : menuOption == 'expectativas' ? expectativas 
        : menuOption == 'sectores' ? sectores 
        : cuentas_publicas;

    for(let i = 0; i < arraySlider.length; i++){
        let bloque = document.createElement('div');
        bloque.setAttribute('id', 'bloque'+ parseInt(areaOrder.indexOf(menuOption)+1) + '-' + parseInt(i+1) +'');
        bloque.setAttribute('class', 'card card--area card--'+menuOption);
        if(menuOption == 'expectativas'){
            if(i == 0 || i ==1 ){
                bloque.setAttribute('class', 'card card--area card--'+menuOption + ' prueba'+parseInt(areaOrder.indexOf(menuOption)+1) + '-' + parseInt(i+1) +'');
            } else {
                bloque.setAttribute('class', 'card card--area card--'+menuOption);
            }
        }
        bloque.setAttribute('data-area', ''+ menuOption + '');
        bloque.setAttribute('data-indicador', '' + arraySlider[i] + '');
        bloque.setAttribute('data-order', '' + parseInt(i+1) + '');

        let bloqueHeader = document.createElement('div');
        bloqueHeader.setAttribute('class', 'card__header card__header--area row row--between row--acenter');

        let prueba = '#chart'+ parseInt(areaOrder.indexOf(menuOption)+1) + '-' + parseInt(i+1) + '';
        
        let titular = document.createElement('h1');
        titular.setAttribute('class', 'card__title card__title--area');
        titular.textContent = '' + commonElementsMap.get(prueba).titulo_area_home + '';

        let enlaceGrafico = document.createElement('a');
        enlaceGrafico.textContent = 'Ver más';
        enlaceGrafico.href = '#';
        enlaceGrafico.setAttribute('class', 'card__link hidden-mobile');
        enlaceGrafico.setAttribute('data-area', ''+ menuOption +'');
        enlaceGrafico.setAttribute('data-indicador', ''+ arraySlider[i] +'');
        enlaceGrafico.setAttribute('data-order', ''+ parseInt(i+1) +'');
        enlaceGrafico.addEventListener('click', function(e){e.preventDefault(); drawSlider(e.target)});

        bloqueHeader.appendChild(titular);
        bloqueHeader.appendChild(enlaceGrafico);

        let bloqueContent = document.createElement('div');
        bloqueContent.setAttribute('class', 'card__content graph');

        let grafico = document.createElement('div');
        grafico.setAttribute('class', 'graph__content');
        grafico.setAttribute('id', 'area-chart'+ parseInt(areaOrder.indexOf(menuOption)+1) + '-' + parseInt(i+1) + '');        

        bloqueContent.appendChild(grafico);

        bloque.appendChild(bloqueHeader);
        bloque.appendChild(bloqueContent);
        area.appendChild(bloque);              
    }

    if(menuOption == 'produccion'){
        getProduccionProdIndustrial(produccionProdIndustrial, 'area');
        getProduccionPibHistograma(produccionPib, 'area');
        getProduccionInversionProductiva(produccionInversionProductiva, 'area');
        getProduccionPibEurozona(produccionPibEurozona, 'area');
        getProduccionEmpresasActivas(produccionEmpresasActivas, 'area');        
        getProduccionPibEmpleo(produccionPibEmpleo, 'area');
    } else if (menuOption == 'empleo'){
        getEmpleoAfiliacionProvincias(empleoAfiliacionProvincias, 'area');
        getEmpleoAfiliacionHistorico(empleoAfiliacionHistorico, 'area');
        getEmpleoTasaParoHistorica(empleoTasaParoHistorica, 'area');
        getEmpleoCaidaEmpleo(empleoCaidaEmpleo, 'area');
        getEmpleoTasaParoEuropa(empleoTasaParoEuropa, 'area');
        getEmpleoNumeroParados(empleoNumeroParados, 'area');
    } else if (menuOption == 'demanda'){
        getDemandaCCAA(demandaMinoristaCCAA, 'area');
        getDemandaSectorial(demandaMinoristaSectorial, 'area');
        getDemandaHistorica(demandaMinoristaHistorica, 'area');       
        getDemandaEurozona(demandaMinoristaEurozona, 'area');
    } else if (menuOption == 'expectativas'){
        getExpectativasPrevisiones(expectativasPrevisiones, 'area');
        getExpectativasPrevisionEconomica(expectativasHogaresEconomia, 'area');
        getExpectativasPrevisionParo(expectativasHogaresParo, 'area');        
        getExpectativasPMI(expectativasPMI, 'area');
        getExpectativasConfianzaConsumidor(expectativasConfianzaConsumidor, 'area');        
    } else if (menuOption == 'sectores'){
        getSectoresCambioAfiliacion(sectoresCambioAfiliacion, 'area');
        getSectoresValorAnadido(sectoresValorAnadido, 'area');
        getSectoresLlegadaViajeros(sectoresLlegadaViajeros, 'area');
    } else if (menuOption == 'cuentas_publicas'){
        getCuentasDeficitCCAA(cuentasDeficitCCAA, 'area');
        getCuentasDeficitHistorico(cuentasDeficitHistorico, 'area');
        getCuentasDeudaPublica(cuentasDeudaPublica, 'area');         
        getCuentasRecaudacion(cuentasRecaudacion, 'area');       
    }
    
    getLastUpdate(menuOption);
    createCarouselArea(menuOption);

    let graficos = document.getElementsByClassName('card__content');
    
    for(let i = 0; i < graficos.length; i++){
        graficos[i].addEventListener('mouseleave', function() {getOutTooltips()})
    }
}

function showSpecificArea(target){
    let areaSeleccionada = target.href.split("#")[1];
    let newTarget = target;
    
        if(areaSeleccionada != 'home'){
            if(areaSeleccionada == currentArea && !isSliderVisible) {
                console.log('Aquí no crearíamos un nuevo carrusel puesto que el usuario habría seleccionado otro indicador del mismo área de análisis');
            } else {                
                if(isMobile()){
                    drawSliderMobile(areaSeleccionada);
                } else {
                    showBlock(areaSeleccionada);
                    createArea(areaSeleccionada);
                    isSliderVisible = false;
                }
                
            }
        } else {
            showBlock('home');
        }

        if(!target.classList.contains('menu__link')){
            let links = document.querySelectorAll('.menu__link');
            for(let i = 0; i < links.length; i++){
                if(links[i].hash == target.hash){
                    newTarget = links[i];
                }
            }
        }

        updateCurrentMenuLink(newTarget);
        if ( isMobile() ) {
            updateMenuPosition(newTarget);
        } else {
            showSubmenu(newTarget);
        }
        currentArea = areaSeleccionada;
        
}

function updateMenuPosition(menuLink) {
    document.getElementsByClassName("menu")[0].scrollLeft = menuLink.offsetLeft - 12;
}

//////
////// Implementación de lógica para carrusel móvil de las cards en la página de Area
//////
let areaCards = document.getElementById("areaCards");
let arrowPrevArea = document.getElementById("prevAreaCard");
let arrowNextArea = document.getElementById("nextAreaCard");
let activeAreaCard;

function createCarouselArea(area) {
    activeAreaCard = 1
    areaCards.style.transform = "translateX(0)";

    /* Botón para ver anterior */
    while(arrowPrevArea.firstChild){
        arrowPrevArea.removeChild(arrowPrevArea.firstChild);
    }

    let arrowPrevAreaImg = document.createElement('img');
    arrowPrevAreaImg.setAttribute('class','block');
    if(area == 'produccion'){
        arrowPrevAreaImg.setAttribute('src','https://www.ecestaticos.com/file/91721a615666f13a905899b3697a4ac4/1593098738-arrow-prev-pink.svg');
    } else if (area == 'empleo'){
        arrowPrevAreaImg.setAttribute('src','https://www.ecestaticos.com/file/8e067c87be997a954e2bb3a06fc176a7/1593099531-arrow-prev-yellow.svg');
    } else if (area == 'demanda'){
        arrowPrevAreaImg.setAttribute('src','https://www.ecestaticos.com/file/657d5fd8e93dacdac8c3250c08330723/1593099610-arrow-prev-green.svg');
    } else if (area == 'expectativas'){
        arrowPrevAreaImg.setAttribute('src','https://www.ecestaticos.com/file/8888d5abc757d24091d679e27b0f2dcf/1593099392-arrow-prev-red.svg');
    } else if (area == 'sectores'){
        arrowPrevAreaImg.setAttribute('src','https://www.ecestaticos.com/file/633c73a7459867536935abae63b2ec6a/1593099667-arrow-prev-blue.svg');
    } else if (area == 'cuentas_publicas'){
        arrowPrevAreaImg.setAttribute('src','https://www.ecestaticos.com/file/07228e6c9e64cdbc428d837dafda9812/1593099464-arrow-prev-orange.svg');
    }
    arrowPrevAreaImg.setAttribute('width','32');
    arrowPrevAreaImg.setAttribute('height','32');
    arrowPrevAreaImg.setAttribute('alt','Flecha izquierda');
    arrowPrevAreaImg.setAttribute('title','Ver anterior');

    arrowPrevArea.appendChild(arrowPrevAreaImg);


    /* Botón para ver siguiente */
    while(arrowNextArea.firstChild){
        arrowNextArea.removeChild(arrowNextArea.firstChild);
    }

    let arrowNextAreaImg = document.createElement('img');
    arrowNextAreaImg.setAttribute('class','block');
    if(area == 'produccion'){
        arrowNextAreaImg.setAttribute('src','https://www.ecestaticos.com/file/c72e2c45ff8a2da7198376621b4cac0c/1593100015-arrow-next-pink.svg');
    } else if (area == 'empleo'){
        arrowNextAreaImg.setAttribute('src','https://www.ecestaticos.com/file/2d5c69f9625678fc6c32b39646ed8bf1/1593100059-arrow-next-yellow.svg');
    } else if (area == 'demanda'){
        arrowNextAreaImg.setAttribute('src','https://www.ecestaticos.com/file/3af021da2e4438784a1e0aba7f93c7d6/1593100107-arrow-next-green.svg');
    } else if (area == 'expectativas'){
        arrowNextAreaImg.setAttribute('src','https://www.ecestaticos.com/file/af20719fc1f55e47a9294422b4691d65/1593100152-arrow-next-red.svg');
    } else if (area == 'sectores'){
        arrowNextAreaImg.setAttribute('src','https://www.ecestaticos.com/file/9c6e1d5689d078bc94ffc692beecd895/1593100205-arrow-next-blue.svg');
    } else if (area == 'cuentas_publicas'){
        arrowNextAreaImg.setAttribute('src','https://www.ecestaticos.com/file/a67bcc8dde41102491035a75dd33f315/1593100251-arrow-next-orange.svg');
    }
    arrowNextAreaImg.setAttribute('width','32');
    arrowNextAreaImg.setAttribute('height','32');
    arrowNextAreaImg.setAttribute('alt','Flecha izquierda');
    arrowNextAreaImg.setAttribute('title','Ver anterior');

    arrowNextArea.appendChild(arrowNextAreaImg);

    /* Dots */
    let cardsAreaDots = document.getElementById("cardsAreaDots");
    while(cardsAreaDots.firstChild){
        cardsAreaDots.removeChild(cardsAreaDots.firstChild);
    }

    for (let i = 0; i < areaCards.childElementCount; i++) {
        let dot = document.createElement('div');
        if ( i==0 ) {
            dot.setAttribute('class','card__dot card__dot--area card__dot--active');
        } else {
            dot.setAttribute('class','card__dot card__dot--area');
        }
        cardsAreaDots.appendChild(dot);
    }

}

arrowPrevArea.addEventListener('click', function(e){
    e.preventDefault();
    activeAreaCard == 1 ? activeAreaCard = areaCards.childElementCount : activeAreaCard--;
    updateAreaCard(activeAreaCard);
    getOutTooltips();
});

arrowNextArea.addEventListener('click', function(e){
    e.preventDefault();
    activeAreaCard == areaCards.childElementCount ? activeAreaCard = 1 : activeAreaCard++;
    updateAreaCard(activeAreaCard);
    getOutTooltips();
});

function updateAreaCard(index) {
    let translate = viewportWidth * (index - 1);
    areaCards.style.transform = "translateX(-"+translate+"px)";
    updateAreaDots(index);
}

function updateAreaDots(index) {
    let cardAreaDots = document.getElementsByClassName('card__dot--area');
    let currentDotActive = document.getElementsByClassName('card__dot--area card__dot--active');
    currentDotActive[0].classList.remove("card__dot--active");
    cardAreaDots[index-1].classList.add("card__dot--active");
}

//////
////// Funciones para crear y manejar el carrusel
//////

//Esta función sólo se llamará cuando alguien pulse un enlace 'Ver más'
function drawSlider(target){    
    //Variables importantes
    let areaSeleccionada = target.getAttribute('data-area');
    let posicionIndicador = parseInt(target.getAttribute('data-order'));

    //Visibilidad de bloques
    document.getElementsByClassName('area')[0].classList.remove('active');
    document.getElementsByClassName('area')[0].classList.add('no-active');
    document.getElementsByClassName('home')[0].classList.remove('active');
    document.getElementsByClassName('home')[0].classList.add('no-active');

    document.getElementsByClassName('slider')[0].classList.remove('no-active');
    document.getElementsByClassName('slider')[0].classList.add('active');
    
    //Llamada funciones
    //Primero, borramos todos los hijos del slider (si ha cambiado de categoría)
    focusSlide = 0;
    
    createSlider(areaSeleccionada, posicionIndicador);
    currentArea = areaSeleccionada;
    isSliderVisible = true;

    //Actualización de submenú activo
    updateCurrentSubmenuLink(posicionIndicador);
}

//Función similar a 'drawSlider' que sólo se utilizará cuando estemos en móvil
function drawSliderMobile(area){
    //Visibilidad de bloques
    document.getElementsByClassName('area')[0].classList.remove('active');
    document.getElementsByClassName('area')[0].classList.add('no-active');
    document.getElementsByClassName('home')[0].classList.remove('active');
    document.getElementsByClassName('home')[0].classList.add('no-active');

    document.getElementsByClassName('slider')[0].classList.remove('no-active');
    document.getElementsByClassName('slider')[0].classList.add('active');
    
    //Llamada funciones
    //Primero, borramos todos los hijos del slider (si ha cambiado de categoría)
    focusSlide = 0;
    
    createSlider(area, 1);
    currentArea = area;
    isSliderVisible = true;

    //Actualización de submenú activo
    //updateCurrentSubmenuLink(1);
}

function updateCurrentSubmenuLink(order) {
    let currentSubmenuLinkActive = document.getElementsByClassName('submenu__link--active')
    if ( currentSubmenuLinkActive.length > 0 ) {
        currentSubmenuLinkActive[0].classList.remove("submenu__link--active");
    }
    let submenuLink = document.getElementsByClassName('submenu--active')[0].children[order-1];
    submenuLink.children[0].classList.add("submenu__link--active");
}

function createSlider(area, posicion) {
    let arraySlider = area == 'produccion' ? produccion 
        : area == 'empleo' ? empleo 
        : area == 'demanda' ? demanda 
        : area == 'expectativas' ? expectativas 
        : area == 'sectores' ? sectores 
        : cuentas_publicas;

    let bloque = document.getElementById('main-slider');

    while(bloque.firstChild){
        bloque.removeChild(bloque.firstChild);
    }

    //Añadimos clase modificadora al slider para saber en qué área nos encontramos
    if ( previousArea != area ) {
        document.getElementsByClassName("slider")[0].classList.remove("slider--"+previousArea);
    }
    document.getElementsByClassName("slider")[0].classList.add("slider--"+area);

    //Cargamos las flechas correspondientes al área en el que nos encontramos
    if(area == 'produccion'){
        document.getElementsByClassName('slider__arrow--prev')[0].setAttribute('src','https://www.ecestaticos.com/file/91721a615666f13a905899b3697a4ac4/1593098738-arrow-prev-pink.svg');
        document.getElementsByClassName('slider__arrow--next')[0].setAttribute('src','https://www.ecestaticos.com/file/c72e2c45ff8a2da7198376621b4cac0c/1593100015-arrow-next-pink.svg');
    } else if (area == 'empleo'){
        document.getElementsByClassName('slider__arrow--prev')[0].setAttribute('src','https://www.ecestaticos.com/file/8e067c87be997a954e2bb3a06fc176a7/1593099531-arrow-prev-yellow.svg');
        document.getElementsByClassName('slider__arrow--next')[0].setAttribute('src','https://www.ecestaticos.com/file/2d5c69f9625678fc6c32b39646ed8bf1/1593100059-arrow-next-yellow.svg');
    } else if (area == 'demanda'){
        document.getElementsByClassName('slider__arrow--prev')[0].setAttribute('src','https://www.ecestaticos.com/file/657d5fd8e93dacdac8c3250c08330723/1593099610-arrow-prev-green.svg');
        document.getElementsByClassName('slider__arrow--next')[0].setAttribute('src','https://www.ecestaticos.com/file/3af021da2e4438784a1e0aba7f93c7d6/1593100107-arrow-next-green.svg');
    } else if (area == 'expectativas'){
        document.getElementsByClassName('slider__arrow--prev')[0].setAttribute('src','https://www.ecestaticos.com/file/8888d5abc757d24091d679e27b0f2dcf/1593099392-arrow-prev-red.svg');
        document.getElementsByClassName('slider__arrow--next')[0].setAttribute('src','https://www.ecestaticos.com/file/af20719fc1f55e47a9294422b4691d65/1593100152-arrow-next-red.svg');
    } else if (area == 'sectores'){
        document.getElementsByClassName('slider__arrow--prev')[0].setAttribute('src','https://www.ecestaticos.com/file/633c73a7459867536935abae63b2ec6a/1593099667-arrow-prev-blue.svg');
        document.getElementsByClassName('slider__arrow--next')[0].setAttribute('src','https://www.ecestaticos.com/file/9c6e1d5689d078bc94ffc692beecd895/1593100205-arrow-next-blue.svg');
    } else if (area == 'cuentas_publicas'){
        document.getElementsByClassName('slider__arrow--prev')[0].setAttribute('src','https://www.ecestaticos.com/file/07228e6c9e64cdbc428d837dafda9812/1593099464-arrow-prev-orange.svg');
        document.getElementsByClassName('slider__arrow--next')[0].setAttribute('src','https://www.ecestaticos.com/file/a67bcc8dde41102491035a75dd33f315/1593100251-arrow-next-orange.svg');
    }   

    let bloqueInterno = document.createElement('div');
    bloqueInterno.setAttribute('id', 'chart-slider');

    //Creamos los gráficos
    for(let i = 0; i < arraySlider.length; i++){
        let prueba = '#chart' + parseInt(areaOrder.indexOf(area)+1) + '-' + parseInt(i+1) + '';

        let bloqueGrafico = document.createElement('div');
        bloqueGrafico.setAttribute('class', 'slide');

        let titulo = document.createElement('h1');
        titulo.setAttribute('class', 'slide__title');
        titulo.textContent = '' + commonElementsMap.get(prueba).titulo_interior + '';

        let subtitulo = document.createElement('h2');
        subtitulo.setAttribute('class', 'slide__subtitle');
        subtitulo.textContent = '' + commonElementsMap.get(prueba).subtitulo_interior + '';

        let grafico = document.createElement('div');
        grafico.setAttribute('id', 'chart' + parseInt(areaOrder.indexOf(area)+1) + '-' + parseInt(i+1) + '');
        grafico.setAttribute('class', 'slide__chart');

        let nota = document.createElement('div');
        nota.setAttribute('class', 'slide__note row row--mobile-column row--end row--mobile-aend');
        
        let fuente = document.createElement('strong');
        fuente.setAttribute('class', 'slide__source');
        fuente.textContent = 'Fuente: ' + commonElementsMap.get(prueba).fuente_interior + '';

        let actualizacion = document.createElement('span');
        actualizacion.setAttribute('class', 'slide__updated');
        let fecha = commonElementsMap.get(prueba).ultima_actualizacion.split("/");        
        actualizacion.textContent = 'Datos actualizados a ' + fecha[0] + ' de ' + meses[+fecha[1] - 1].toLocaleLowerCase() + ' de ' + fecha[2];

        nota.appendChild(fuente);
        nota.appendChild(actualizacion);

        bloqueGrafico.appendChild(titulo);
        bloqueGrafico.appendChild(subtitulo);
        bloqueGrafico.appendChild(grafico);
        bloqueGrafico.appendChild(nota);        

        bloqueInterno.appendChild(bloqueGrafico);
    }

    //Creamos los puntitos
    let bloquePuntos = document.createElement('div');
    bloquePuntos.setAttribute('id', 'slider-dots');
    bloquePuntos.setAttribute('class', 'slider__dots');

    for(let i = 0; i < arraySlider.length; i++){
        let punto = document.createElement('span');
        punto.setAttribute('class', 'slider__dot');
        punto.addEventListener('click', function(e){currentSlide(i+1);getOutTooltips();});

        bloquePuntos.appendChild(punto);
    }

    bloque.appendChild(bloqueInterno);
    bloque.appendChild(bloquePuntos);

    slideWidth = parseInt(d3.select('.slide__chart').style("width")),
    slideHeight = parseInt(d3.select('.slide__chart').style("height"));

    //Dibujo de gráficos en sus respectivos carruseles
    //Llamamos a las funciones para pintar los gráficos del área de análisis --> Faltaría darle una pensada a títulos, subtítulos y fuente
    if(area == 'produccion'){
        getProduccionProdIndustrial(produccionProdIndustrial, 'slider');
        getProduccionPibHistograma(produccionPib, 'slider');
        getProduccionInversionProductiva(produccionInversionProductiva, 'slider');
        getProduccionPibEurozona(produccionPibEurozona, 'slider');
        getProduccionEmpresasActivas(produccionEmpresasActivas, 'slider');
        getProduccionPibEmpleo(produccionPibEmpleo, 'slider');
    } else if (area == 'empleo'){
        getEmpleoAfiliacionProvincias(empleoAfiliacionProvincias, 'slider');
        getEmpleoAfiliacionHistorico(empleoAfiliacionHistorico, 'slider');
        getEmpleoTasaParoHistorica(empleoTasaParoHistorica, 'slider');
        getEmpleoCaidaEmpleo(empleoCaidaEmpleo, 'slider');
        getEmpleoTasaParoEuropa(empleoTasaParoEuropa, 'slider');
        getEmpleoNumeroParados(empleoNumeroParados, 'slider');
    } else if (area == 'demanda'){
        getDemandaCCAA(demandaMinoristaCCAA, 'slider');
        getDemandaHistorica(demandaMinoristaHistorica, 'slider');
        getDemandaSectorial(demandaMinoristaSectorial, 'slider');        
        getDemandaEurozona(demandaMinoristaEurozona, 'slider');
    } else if (area == 'expectativas'){
        getExpectativasPrevisiones(expectativasPrevisiones, 'slider');
        getExpectativasPMI(expectativasPMI, 'slider');
        getExpectativasConfianzaConsumidor(expectativasConfianzaConsumidor);
        getExpectativasPrevisionEconomica(expectativasHogaresEconomia, 'slider');
        getExpectativasPrevisionParo(expectativasHogaresParo, 'slider');
    } else if (area == 'sectores'){
        getSectoresCambioAfiliacion(sectoresCambioAfiliacion, 'slider');
        getSectoresValorAnadido(sectoresValorAnadido, 'slider');
        getSectoresLlegadaViajeros(sectoresLlegadaViajeros, 'slider');
    } else if (area == 'cuentas_publicas'){
        getCuentasDeficitCCAA(cuentasDeficitCCAA, 'slider');
        getCuentasDeficitHistorico(cuentasDeficitHistorico, 'slider');
        getCuentasRecaudacion(cuentasRecaudacion, 'slider');
        getCuentasDeudaPublica(cuentasDeudaPublica, 'slider');        
    }
    showSlide(posicion);

    let graficos = document.getElementsByClassName('slide__chart');
    
    for(let i = 0; i < graficos.length; i++){
        graficos[i].addEventListener('mouseleave', function() {getOutTooltips()})
    }

    previousArea = area;
}

function showSlide(n) {
    let slides = document.getElementsByClassName('slide');
    let dots = document.getElementsByClassName('slider__dot');
    let newSlide = n;

    if(n > slides.length){
        newSlide = 1;
    }
    if(n < 1){
        newSlide = slides.length;
    }

    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }

    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" slider__dot--active", "");
    }

    slides[newSlide - 1].style.display = "block";
    dots[newSlide - 1].className += " slider__dot--active";

    focusSlide = newSlide;

    if(!isMobile()){
        updateCurrentSubmenuLink(focusSlide);
    }    
}

function plusSlide(n) {
    let newSlide = parseInt(focusSlide) + n;
    showSlide(newSlide);
}

function currentSlide(n){
    showSlide(n);
}

//Damos eventos a las flechas en un primer momento
document.getElementsByClassName('slider__arrow--prev')[0].addEventListener('click', function(){plusSlide(-1);getOutTooltips();});
document.getElementsByClassName('slider__arrow--next')[0].addEventListener('click', function(){plusSlide(1);getOutTooltips();});

let innerSlider = document.getElementById("main-slider");
if ( isMobile() ) {
    innerSlider.addEventListener('swiped-left', function(evt) {
        plusSlide(1);
    });
    innerSlider.addEventListener('swiped-right', function(evt) {
        plusSlide(-1);
    });
}

//Producción
function getProduccionPibHistograma(data, nivelNavegacion){ //Histograma
    let grafico = nivelNavegacion == 'area' ? '#area-chart1-1' : '#chart1-1';
    let lineaEjeX = nivelNavegacion == 'area' ? 'area-crecimiento-trim-ejeX' : 'slider-crecimiento-trim-ejeX';
    
    //Estructura fundamental del gráfico
    let margin = {top: 5, right: 10, bottom: 20, left: 30},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;
    
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top +')');

    //Eje X
    let x = d3.scaleBand()
        .range([0,width])
        .domain(d3.range(data.length));

    let x2 = d3.scaleTime()
        .range([0,width])
        .domain(d3.extent(data, function(d) { return formatTime(d.trimestre_v2) }));

    let xAxis = function(svg){
        svg.call(d3.axisBottom(x2).ticks(8).tickFormat(function(d){ return d.getFullYear(); }))
        svg.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {if(d.getFullYear() == 1996 || d.getFullYear() == 2004 || d.getFullYear() == 2012 ||d.getFullYear() == 2020){return 1} else {return 0}})})
        svg.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d) {return d.getFullYear() == 2020 ? 'end' : 'middle' })})
        svg.call(function(g){g.selectAll('line').remove()})
        svg.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')});
    }        

    chart.append('g')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);
    
    //Eje Y
    let y0 = Math.max(Math.abs(d3.min(data, function(d) {return d.pib_data})), Math.abs(d3.max(data, function(d) {return d.pib_data})));

    let y = d3.scaleLinear()
        .domain([-y0,2])
        .range([height,0])
        .nice();

    let yAxis = function(svg){
        svg.call(d3.axisLeft(y).ticks(6))
        svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
        svg.call(function(g){g.select('.domain').remove()})
        svg.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 0.5)
            .attr("stroke", function(d) {return d == 0 ? '#d9d9d9' : '#343434'})
            .attr("x1", '0%')
            .attr("x2", '' + (document.getElementById('' + lineaEjeX + '').getBoundingClientRect().width) + '')});
    }        

    chart.append('g')
        .attr("transform", 'translate(0,0)')
        .call(yAxis);

    chart.selectAll('.barras')
        .data(data)
        .enter()
        .append('rect')
        .attr('fill', function(d) {return d.pib_data < 0 ? '#fde5e1' : '#ffbec3'})
        .attr('y', function(d) { return y(Math.max(0,d.pib_data)) })
        .attr('x', function(d,i) { return x(i)})
        .attr('height', function(d) {return Math.abs(y(d.pib_data) - y(0));})
        .attr('width', x.bandwidth() / 2)
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipProduccion.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipProduccion.html('<span style="font-weight: 300;">' + d.trimestre + '</span><br><span style="font-weight: 900;">' + d.pib_data.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipProduccion.style('display','none').style('opacity','0');
        })
}

function getProduccionInversionProductiva(data, nivelNavegacion){ //Gráfico de fiebre
    let grafico = nivelNavegacion == 'area' ? '#area-chart1-2' : '#chart1-2';

    //Estructura fundamental del gráfico
    let margin = {top: 0, right: 10, bottom: 20, left: 30},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;
    
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', 'translate('+ margin.left + ',' + margin.top + ')');

    //Eje X
    let x = d3.scaleBand()
        .domain(data.map(function(d) { return d.trimestre}))
        .range([0, width])
        .padding(0.1);
    
    let xAxis = function(svg){
        svg.call(d3.axisBottom(x).tickFormat(function(d) { return d.substr(-4)}).tickValues(x.domain().filter(function(d,i) { return !(i%32) })))
        svg.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d) {return d.substr(-4) == 1995 ? 'start' : d.substr(-4) == 2019 ? 'end' : 'middle'})})
        svg.call(function(g){g.selectAll('.tick line').remove()});
    }        
    
    chart.append('g')
        .attr('transform', 'translate(0, ' + height +')')
        .call(xAxis);

    //Eje Y
    let y = d3.scaleLinear()
        .domain(d3.extent(data, function(d) {return d.ip_data}))
        .range([height,0]);

    let yAxis = function(svg){
        svg.attr("transform", 'translate(0,0)')
        svg.call(d3.axisLeft(y).ticks(7))
        svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
        svg.call(function(g){g.selectAll('.tick line').remove()});
    }        
    
    chart.append("g")
        .attr('transform', 'translate(0, 0)')
        .call(yAxis);

    //Line
    let line = d3.line()
        .defined(function(d) { return !isNaN(d.ip_data)})
        .x(function(d) { return x(d.trimestre)})
        .y(function(d) { return y(d.ip_data)});
    
    chart.append("path")
        .datum(data.filter(line.defined()))
        .attr("fill", "none")
        .attr("stroke", "#ffbec3")
        .attr("stroke-width", 1)
        .attr('transform', 'translate(0, 0)')
        .attr("d", line);

    //Area > set the gradient
    chart.append("linearGradient")				
    .attr("id", nivelNavegacion == 'area' ? 'area-gradient' : 'slider-gradient')			
    .attr("gradientUnits", "userSpaceOnUse")	
    .attr("x1", 0).attr("y1", y(d3.min(data, function(d) {return d.ip_data})))			
    .attr("x2", 0).attr("y2", y(d3.max(data, function(d) {return d.ip_data})))		
    .selectAll("stop")						
    .data([
        {offset: "0%", color: "rgba(0, 0, 0, 0)"},
        {offset: "90%", color: "rgba(255, 190, 195, 0.31)"},
        {offset: "100%", color: "rgba(255, 190, 195, 1)"}	
    ])					
    .enter()
    .append("stop")			
    .attr("offset", function(d) { return d.offset; })	
    .attr("stop-color", function(d) { return d.color; });

    let area = d3.area()
        .x(function(d) { return x(d.trimestre); })	
        .y0(height)					
        .y1(function(d) { return y(d.ip_data); });

    chart.append('path')
        .datum(data)
        .attr('fill', nivelNavegacion == 'area' ? 'url(#area-gradient)' : 'url(#slider-gradient)')
        .attr('d', area);

    //Rectángulo para tooltip
    chart.selectAll('.circles')
        .data(data)
        .enter()
        .append('circle')
        .attr("r", 5)
        .attr("cx", function(d) { return x(d.trimestre); })
        .attr("cy", function(d) { return y(d.ip_data); })
        .style("fill", '#000')
        .style('opacity', '0')
        .attr('transform', 'translate(2.5, 0)')
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipProduccion.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipProduccion.html('<span style="font-weight: 300;">' + d.trimestre + '</span><br><span style="font-weight: 900;">' + d.ip_data.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipProduccion.style('display','none').style('opacity','0');
        })
}

function getProduccionPibEurozona(data, nivelNavegacion){ //Barras Eurozona
    let grafico = nivelNavegacion == 'area' ? '#area-chart1-3' : '#chart1-3';
    let contenedor = document.getElementById('chart1-3');
    let lineaEjeY = nivelNavegacion == 'area' ? 'area-produccion-pib-ue-ejeY' : 'slider-produccion-pib-ue-ejeY';

    //Primero, desarrollo y dibujo de los botones
    let menosAltura = 0;
    let boton = 'primer_trimestre';
    //Si estamos en área no habrá botones. Si no estamos en área, sí
    if(nivelNavegacion != 'area'){
        menosAltura = 72.5;       

        // Botones en Interior
        let grafico2 = document.getElementById('chart1-3');
        let grupoBotones = document.createElement('div');
        grupoBotones.classList.add('chart-buttons');

        let firstButton = document.createElement('button');
        firstButton.textContent = 'T1 2020';
        firstButton.classList.add('button');
        firstButton.classList.add('button__produccion');
        firstButton.classList.add('button-active');
        let secondButton = document.createElement('button');
        secondButton.textContent = 'T2 2020';
        secondButton.classList.add('button');
        secondButton.classList.add('button__produccion');

        grupoBotones.appendChild(firstButton);
        grupoBotones.appendChild(secondButton);
        grafico2.insertBefore(grupoBotones, grafico2.firstChild);

        //Asociamos los botones a un evento que cambiará la forma de datos
        firstButton.addEventListener('click', function() {
            updateChart('primer_trimestre');
            boton = 'primer_trimestre';
            firstButton.classList.add('button-active');
            secondButton.classList.remove('button-active');
            getOutTooltips();
        });
        secondButton.addEventListener('click', function() {
            updateChart('segundo_trimestre');
            boton = 'segundo_trimestre';
            secondButton.classList.add('button-active');
            firstButton.classList.remove('button-active');
            getOutTooltips();
        });
    }

    //Estructura de datos
    let newData = [];

    if(nivelNavegacion == 'area'){
        for(let i = 0; i < data.length; i++){
            if(data[i].pais == 'UE' || data[i].pais == 'Eurozona' || data[i].pais == 'Alemania' || data[i].pais == 'Grecia' || data[i].pais == 'España' || data[i].pais == 'Francia' ||
            data[i].pais == 'Italia' || data[i].pais == 'P. Bajos' || data[i].pais == 'Portugal' || data[i].pais == 'Reino Unido' || data[i].pais == 'Austria'){
                newData.push(data[i]);
            }
        }
    } else {
        newData = data.slice();      
    }
    
    //Estructura fundamental del gráfico
    let margin = {top: 0, right: 15, bottom: 20, left: 95},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom - menosAltura;

    let chart = d3.select(grafico)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // Y axis
    let y = d3.scaleBand()
    .range([0, height])
    .domain(newData.map(function(d) { return d.pais; }));

    let yAxis = function(g){
        g.call(d3.axisLeft(y))
        g.call(function(g){g.selectAll('.domain').attr('id', lineaEjeY).style('opacity','0')})
        g.call(function(g){g.selectAll('.tick text').style('font-size', nivelNavegacion == 'area' ? '15px' : '14px').style('opacity','1').style('font-weight','300')})
        g.call(function(g){g.selectAll('.tick line').remove()});
    }        

    chart.append("g")
    .call(yAxis);

    //X axis
    let x = d3.scaleLinear()
    .range([0, width])
    .domain([-6, 2]);
    
    let xAxis = function(g){
        g.call(d3.axisBottom(x).ticks(5))
        g.call(function(g){g.selectAll('.domain').remove()})
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d){return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 0.5)
            .attr("stroke", "#343434")
            .attr("stroke-width", "1")
            .attr("y1", '0%')
            .attr("y2", '-' + (document.getElementById(lineaEjeY).getBoundingClientRect().height) + '')})
        g.attr("transform", "translate(0," + (height - 5) + ")");
    }
    
    chart.append("g")
        .attr('class','x-axis')
        .call(xAxis);

    function initChart(){
        // Lines
        chart.selectAll("myline")
            .data(newData)
            .enter()
            .append("line")
            .attr('class', 'mylines')
                .attr("x1", function(d) { return x(0); })
                .attr("x2", function(d) { return !isNaN(d.dato_trimestre) ? x(d.dato_trimestre) : x(0); })
                .attr("y1", function(d) { return y(d.pais) + (y.bandwidth() / 2); })
                .attr("y2", function(d) { return y(d.pais) + (y.bandwidth() / 2); })
                .attr("stroke", "#ffbec3")
                .attr("stroke-width", "4px")
                .on('touchstart mouseover', function(d){
                    let dato = boton == 'primer_trimestre' ? d.dato_trimestre : d.dato_trimestre_2;

                    let bodyWidth = parseInt(d3.select('body').style('width'));
                    let mapHeight = parseInt(d3.select('body').style('height'));
            
                    tooltipProduccion.transition()     
                        .duration(200)
                        .style('display', 'block')
                        .style('opacity','0.9')
                    tooltipProduccion.html('<span style="font-weight: 900;">' + dato.toFixed(2).replace('.',',') + '%</span>')
                        .style("left", ""+ (d3.event.pageX - 25) +"px")     
                        .style("top", ""+ (d3.event.pageY - 35) +"px");
                })
                .on('touchend mouseout mouseleave', function(d) {
                    tooltipProduccion.style('display','none').style('opacity','0');
                })
    }

    function updateChart(buttonType){
        buttonType == 'primer_trimestre' ? x.domain([-6, 2]) : x.domain([-20, 5]);

        let chart = d3.select(grafico).transition();

        chart.select('.x-axis')
            .duration(750)
            .call(xAxis);

        chart.selectAll('.mylines')
            .duration(750)
            .attr("x1", function(d) { return x(0); })
            .attr("x2", function(d) {
                if(buttonType == 'primer_trimestre'){
                    return !isNaN(d.dato_trimestre) ? x(d.dato_trimestre) : x(0); 
                } else {
                    return !isNaN(d.dato_trimestre_2) ? x(d.dato_trimestre_2) : x(0);                 
                }               
            });
    }

    if(nivelNavegacion != 'area'){
        //Nota aclaratoria
        let nota_aclaratoria = document.createElement('div');
        nota_aclaratoria.setAttribute('class', 'nota-aclaratoria-produccion');
        nota_aclaratoria.textContent = '* No hay datos disponibles para los países sin barra asociada';
        contenedor.appendChild(nota_aclaratoria);  
    }

    initChart();
}

function getProduccionEmpresasActivas(data, nivelNavegacion){ //Gráfico de barras
    let grafico = nivelNavegacion == 'area' ? '#area-chart1-4' : '#chart1-4';
    let newData = nivelNavegacion == 'area' ? data.sort(function(a,b) { return a.cambio_data - b.cambio_data}).slice(0,10) : data;
    let lineaEje = nivelNavegacion == 'area' ? 'area-empresasActivasEjeY' : 'slider-empresasActivasEjeY';

    //Estructura fundamental del gráfico
    let margin = {top: 25, right: 25, bottom: 20, left: 125},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : (slideWidth - 20)) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : 1200) - margin.top - margin.bottom;

    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // Y axis
    let y = d3.scaleBand()
    .range([0, height])
    .domain(newData.map(function(d) { return d.provincia; }))
    .paddingInner(.8)
    .align(1);

    let yAxis = function(svg){
        svg.call(d3.axisLeft(y))
        svg.call(function(g){g.selectAll('.domain').attr('id', lineaEje).style('opacity','0')})
        svg.call(function(g){g.selectAll('.tick text').style('font-size','15px').style('opacity','1').style('font-weight','300')})
        svg.call(function(g){g.selectAll('.tick line').remove()});
    }        

    chart.append("g")
        .call(yAxis);

    //X axis
    let x = d3.scaleLinear()
        .domain([d3.min(newData, function(d) { return d.cambio_data - 1}), newData.length > 10 ? d3.max(newData, function(d) { return d.cambio_data}) : 0])
        .range([0, width]);

    let xAxis = function(svg){
        svg.call(d3.axisBottom(x).ticks(nivelNavegacion == 'area' ? 3 : 6))
        svg.call(function(g){g.selectAll('.domain').remove()})
        svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        svg.call(function(g){g.selectAll('.tick line')
            .attr('y1','0')
            .attr('y2','-' + (document.getElementById(lineaEje).getBoundingClientRect().height) +'')
            .style('stroke','#343434')
            .style('stroke-dasharray','2')})
        svg.attr("transform", "translate(0," + (height + 2.5) + ")");
    }
    
    let xAxis2 = function(svg){
        svg.call(d3.axisTop(x).ticks(nivelNavegacion == 'area' ? 3 : 6))
        svg.call(function(g){g.selectAll('.domain').remove()})
        svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
    }  

    chart.append("g")
        .call(xAxis);

    chart.append("g")
        .call(xAxis2);

    chart.selectAll('.bar-chart')
        .data(newData)
        .enter()
        .append('rect')
        .attr('fill', function(d) {return d.cambio_data < 0 ? '#ffbec3' : '#fde5e1'})
        .attr('x', function(d) { return x(Math.min(0, d.cambio_data))})
        .attr('y', function(d) { return y(d.provincia) })
        .attr('width', function(d) { return Math.abs(x(d.cambio_data) - x(0))})
        .attr('height', y.bandwidth())
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipProduccion.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipProduccion.html('<span style="font-weight: 900;">' + d.cambio_data.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px"); 
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipProduccion.style('display','none').style('opacity','0');
        })    
}

function getProduccionProdIndustrial(data, nivelNavegacion){ //Mapa por CCAA
    getThisMap(data);

    function getThisMap(data){
        //Primero borramos todos los hijos
        // let slider = document.getElementById('chart1-5');
        // while(slider.firstChild){
        //     slider.removeChild(slider.firstChild);
        // }

        let grafico = nivelNavegacion == 'area' ? '#area-chart1-5' : '#chart1-5';

        //Creación de leyenda        
        let divLeyenda = document.createElement('div');
        divLeyenda.setAttribute('class','legend');

        let primerPunto = document.createElement('p');
        primerPunto.setAttribute('class','legend__item legend__item--produccion-primero');
        primerPunto.textContent = 'Menos de -20';

        let segundoPunto = document.createElement('p');
        segundoPunto.setAttribute('class','legend__item legend__item--produccion-segundo');
        segundoPunto.textContent = '-20 a -10';

        let tercerPunto = document.createElement('p');
        tercerPunto.setAttribute('class','legend__item legend__item--produccion-tercero');
        tercerPunto.textContent = '-10 a 0';

        let cuartoPunto = document.createElement('p');
        cuartoPunto.setAttribute('class','legend__item legend__item--produccion-cuarto');
        cuartoPunto.textContent = 'Más de 0';

        divLeyenda.appendChild(primerPunto);
        divLeyenda.appendChild(segundoPunto);
        divLeyenda.appendChild(tercerPunto);
        divLeyenda.appendChild(cuartoPunto);

        document.getElementById(grafico.substr(1)).appendChild(divLeyenda);

        d3.json(mapaAutonomias, function(error, mapa){
            if(error) throw error;
            //No hay datos para Ceuta y Melilla, por lo que no es necesario recuperarlas
            let coordenadasMapa = topojson.feature(mapa, mapa.objects.autonomous_regions);
            let datosMapa = coordenadasMapa.features;
    
            data.forEach(function(item) {
                datosMapa.forEach(function(item2) { //Sólo quedan sin datos Ceuta y Melilla
                    if(item.id_ccaa == parseInt(item2.id)){
                        item2.ccaa = item.ccaa;
                        item2.pi_data = item.pi_data;
                    }
                })
            });    
            
            //Estructura fundamental del gráfico
            let margin = {top: 20, right: 20, bottom: 20, left: 30},
            width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth),
            height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - (window.innerWidth < 425 ? 50 : 25);
                
            //Inicialización del gráfico
            let chart = d3.select(grafico)
                .append('svg')
                .attr('width', width)
                .attr('height', height);
    
            const projection = d3.geoConicConformalSpain().fitSize([width,height], coordenadasMapa);
            const path = d3.geoPath(projection);
    
            chart.selectAll(".region")
                .data(datosMapa)
                .enter()
                .append("path")
                .attr('id', function(d) {return d.ccaa})
                .attr("d", path)
                .style("fill", function(d) {return d.pi_data < -20 ? '#4d393a' : d.pi_data < -10 && d.pi_data >= -20 ? '#9d7779' : d.pi_data < 0 && d.pi_data >= -10 ? '#ffbec3' : '#ffebec'})
                .style("stroke", "#000")
                .style("stroke-width", ".6px")
                .on('touchstart mouseover', function(d) {
                    let bodyWidth = parseInt(d3.select('body').style('width'));
                    let mapHeight = parseInt(d3.select('body').style('height'));
            
                    // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
                    // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
            
                    tooltipProduccion.transition()     
                        .duration(200)
                        .style('display', 'block')
                        .style('opacity','0.9')
                    tooltipProduccion.html('<span style="font-weight: 300;">' + d.ccaa + '</span><br><span style="font-weight: 900;">' + d.pi_data.toFixed(2).replace('.',',') + '%</span>')
                        .style("left", ""+ (d3.event.pageX - 25) +"px")     
                        .style("top", ""+ (d3.event.pageY - 35) +"px");
                })
                .on('touchend mouseout mouseleave', function(d) {
                    tooltipProduccion.style('display','none').style('opacity','0');
                })
        })

        // d3.json(mapaAutonomias).then(function(mapa) {
            
        // }).catch(function(error){console.log(error)});
    }

    function getThisBar(){
        //Primero borramos todos los hijos
        let slider = document.getElementById('chart1-5');
        while(slider.firstChild){
            slider.removeChild(slider.firstChild);
        }
        
        let grafico = nivelNavegacion == 'area' ? '#area-chart1-5' : '#chart1-5';
        //Estructura fundamental del gráfico
        let margin = {top: 20, right: 20, bottom: 20, left: 30},
        width = nivelNavegacion == 'area' ? areaWidth : slideWidth - margin.left - margin.right,
        height = nivelNavegacion == 'area' ? areaHeight : slideHeight - margin.top - margin.bottom;
        
        let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', 'translate(' + margin.left + ', 10)');

        var x = d3.scaleBand()
            .range([0, width])
            .domain(data.map(function(d) { return d.ccaa; }))
            .padding(0.1)

        chart.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x))
            .selectAll("text")
            .attr("transform", "translate(-10,0)rotate(-45)")
            .style("text-anchor", "end");

        var y = d3.scaleLinear()
              .domain([d3.min(data, function(d) {return d.pi_data}) - 1,d3.max(data, function(d) {return d.pi_data}) + 1])
              .range([height,0]);

        var yAxisScale = d3.scaleLinear()
            .domain([d3.min(data), d3.max(data)])
            .range([height - y(d3.min(data)), 0 ]);
        
        chart.append("g")
              .call(d3.axisLeft(y));

        chart.selectAll("bar")
            .data(data)
            .enter()
            .append("rect")
            .style("fill", function(d) { return d.pi_data > 0 ? '#fcf1d1' : '#ffda62' })
            .attr('x', function(d) {
                return x(d.ccaa);
            })
            .attr('y', function(d) {
            return y(Math.max(0, d.pi_data));
            })
            .attr('height', function(d) {
            return Math.abs(y(d.pi_data) - y(0));
            })
            .attr('width', x.bandwidth() - 7.5);

        // chart.selectAll("myline")
        // .data(data)
        // .enter()
        // .append("line")
        // .attr("x1", function(d) { return x(d.ccaa); })
        // .attr("x2", function(d) { return x(d.ccaa); })
        // .attr("y1", function(d) { return y(d.pi_data); })
        // .attr("y2", y(0))
        // .attr("stroke", "grey");

        // chart.selectAll("mycircle")
        //     .data(data)
        //     .enter()
        //     .append("circle")
        //         .attr("cx", function(d) { return x(d.ccaa); })
        //         .attr("cy", function(d) { return y(d.pi_data); })
        //         .attr("r", "4")
        //         .style("fill", "#69b3a2")
        //         .attr("stroke", "black");
    }       
}

function getProduccionPibEmpleo(data, nivelNavegacion){ //Gráfico de dispersión    
    let grafico = nivelNavegacion == 'area' ? '#area-chart1-6' : '#chart1-6';

    let contenedor = document.getElementById(grafico.substr(1));
    if(nivelNavegacion != 'area'){
        let ejeY = document.createElement('div');
        ejeY.setAttribute('class', 'eje-grafico-dispersion');
        ejeY.textContent = 'PIB trimestral';
        contenedor.appendChild(ejeY);
    }    

    //Estructura fundamental del gráfico
    let margin = {top: 20, right: 7.5, bottom: 20, left: 27.5},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom - (nivelNavegacion == 'area' ? 0 : 44);

    let lineaEjeY = nivelNavegacion == 'area' ? 'area-pibempleoEjeY' : 'slider-pibempleoEjeY';
    let lineaEjeX = nivelNavegacion == 'area' ? 'area-pibempleoEjeX' : 'slider-pibempleoEjeX';
    
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
    
    //Eje x
    let x = d3.scaleLinear()
        .domain([d3.min(data, function(d) { return d.horas_efectivas_data - 1}),d3.max(data, function(d) { return d.horas_efectivas_data + 1})])
        .range([0, width]);
    
    let xAxis = function(svg){
        svg.call(d3.axisBottom(x).ticks(5))
        svg.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')})
        svg.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 1)
            .attr("stroke",function(d) {return d == 0 ? '#d9d9d9' : '#151515'})
            .attr("stroke-width",function(d) {return d == 0 ? '1' : '1'})
            .attr("y1", '0%')
            .attr("y2", nivelNavegacion == 'area' ? '-87%' : '-94.15%')})
        svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})});
    }        

    chart.append('g')
        .attr('transform', 'translate(0,' + height + ')')        
        .call(xAxis);
    
    //Eje y
    let y = d3.scaleLinear()
        //.domain([d3.min(data, (d) => { return d.pib_correlacion_data - 1}),d3.max(data, (d) => { return d.pib_correlacion_data + 1})])
        .domain([-19,2])
        .range([height, 0]);

    let yAxis = function(svg){
        svg.call(d3.axisLeft(y).ticks(5))
        svg.attr('id', nivelNavegacion == 'area' ? 'area-pibempleoEjeY' : 'slider-pibempleoEjeY')
        svg.call(function(g){g.select('.domain').remove()})
        svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        svg.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 1)
            .attr("stroke", function(d) {return d == 0 ? '#d9d9d9' : '#151515'})
            .attr("stroke-width", function(d) {return d == 0 ? '1' : '1'})
            .attr("x1", '0%')
            .attr("x2", '' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')});
    }        

    chart.append('g')
        .attr('transform', 'translate(0,0)')
        .call(yAxis)

    //Línea de regresión
    let datosX = data.map(function(d) { return d.horas_efectivas_data; });
    let datosY = data.map(function(d) { return d.pib_correlacion_data; });
    let regresion = calculateRegression(datosX, datosY);

    let line = d3.line()
        .x(function(d) {
            return x(d.horas_efectivas_data);
        })
        .y(function(d) {
            return y(regresion(d.horas_efectivas_data));
        });

    chart.append("path")
        .datum(data)
        .attr("stroke",'#343434')
        .attr("stroke-width", '1.5px')
        .attr("fill", "none")
        .attr("d", line);

    //Dibujo del gráfico
    chart.append('g')
        .selectAll("dot")
        .data(data)
        .enter()
        .append('circle')
        .style('opacity', '0.8')
        .attr('r', nivelNavegacion == 'area' ? 3.5 : 6)
        .style('fill', '#ffbec3')
        .attr('cx', function(d) { return x(d.horas_efectivas_data) })
        .attr('cy', function(d) { return y(d.pib_correlacion_data) })
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));

            tooltipProduccion.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipProduccion.html('<span style="font-weight: 300;">Trimestre:</span> <span style="font-weight: 900;">' + d.trimestre + '</span><br><span style="font-weight: 300;">PIB:</span> <span style="font-weight: 900;">' + d.pib_correlacion_data.toFixed(2).replace('.',',') + '</span><br><span style="font-weight: 300;">Horas de trabajo:</span><br><span style="font-weight: 900;">' + d.horas_efectivas_data.toFixed(2).replace('.',',') + '</span>')
                .style("left", ""+ (d3.event.pageX - 40) +"px")     
                .style("top", ""+ (d3.event.pageY - 50) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipProduccion.style('display','none').style('opacity','0');
        });

    if(nivelNavegacion != 'area'){
        let ejeX = document.createElement('div');
        ejeX.setAttribute('class', 'eje-grafico-dispersion eje-grafico-dispersion--text-right'); 
        ejeX.textContent = 'Horas trabajadas';
        contenedor.appendChild(ejeX);
    }    
}

//Expectativas
function getExpectativasPrevisionEconomica(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart2-1' : '#chart2-1';

    //Estructura fundamental del gráfico
    let margin = {top: 0, right: 5, bottom: 20, left: 30},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;
    
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    //Eje X
    let x = d3.scaleBand()
        .range([0,width])
        .domain(d3.range(data.length));

    let x2 = d3.scaleBand()
        .range([0,width])
        .domain(data.map((function(d) { return d.mes })));

    let xAxis = function(svg){
        svg.call(d3.axisBottom(x2).tickFormat(function(d,i) {if(i == 0){return '2007'} else if(i == data.length -1){ return '2020'}}))
        svg.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d,i) {return i == 0 ? 'start' : i == data.length -1 ? 'end' : 'middle' })})
        svg.call(function(g){g.selectAll('.tick line').remove()});
    }        

    chart.append('g')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);
    
    //Eje Y
    let y = d3.scaleLinear()
        .domain([d3.min(data, function(d) {return d.situacion_economica}),d3.max(data, function(d) {return d.situacion_economica})])
        .range([height,0])
        .nice();

    let yAxis = function(svg){
        svg.call(d3.axisLeft(y).ticks(6))
        svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        svg.call(function(g){g.select('.domain').remove()});
    }        

    chart.append('g')
        .call(yAxis);

    //Histograma
    chart.selectAll('.barras')
        .data(data)
        .enter()
        .append('rect')
        .attr('fill', function(d) {return d.situacion_economica < 0 ? '#ff4a4a' : '#ffc8c8'})
        .attr('y', function(d) { return y(Math.max(0,d.situacion_economica)) })
        .attr('x', function(d,i) { return x(i)})
        .attr('height', function(d) {return Math.abs(y(d.situacion_economica) - y(0));})
        .attr('width', x.bandwidth() / 2)
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipExpectativas.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipExpectativas.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><br><span style="font-weight: 900;">' + d.situacion_economica.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipExpectativas.style('display','none').style('opacity','0');
        })
}

function getExpectativasPrevisionParo(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart2-2' : '#chart2-2';

    //Estructura fundamental del gráfico
    let margin = {top: 20, right: 5, bottom: 20, left: 30},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;
    
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleBand()
        .range([0,width])
        .domain(d3.range(data.length));

    let x2 = d3.scaleBand()
        .range([0,width])
        .domain(data.map(function(d) { return d.mes }))

    let xAxis = function(g){
        g.call(d3.axisBottom(x2).tickFormat(function(d,i) {if(i == 0){return '2007'} else if(i == data.length -1){ return '2020'}}))
        g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d,i) {return i == 0 ? 'start' : i == data.length -1 ? 'end' : 'middle' })})
        g.call(function(g){g.selectAll('.tick line').remove()});
    }        

    chart.append('g')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);
    
    //Eje Y
    let y = d3.scaleLinear()
        .domain([d3.min(data, function(d) {return d.situacion_empleo}),d3.max(data, function(d) {return d.situacion_empleo})])
        .range([height,0])
        .nice();

    let yAxis = function(g){
        g.call(d3.axisLeft(y).ticks(6))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d){return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.select('.domain').remove()});
    }        

    chart.append('g')
        .call(yAxis);

    chart.selectAll('.barras')
        .data(data)
        .enter()
        .append('rect')
        .attr('fill', function(d) {return d.situacion_empleo < 0 ? '#ff4a4a' : '#ffc8c8'})
        .attr('y', function(d) { return y(Math.max(0,d.situacion_empleo)) })
        .attr('x', function(d,i) { return x(i)})
        .attr('height', function(d) {return Math.abs(y(d.situacion_empleo) - y(0));})
        .attr('width', x.bandwidth() / 2)
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipExpectativas.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipExpectativas.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><br><span style="font-weight: 900;">' + d.situacion_empleo.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d){
            tooltipExpectativas.style('display','none').style('opacity','0');
        })
}

function getExpectativasPrevisiones(data, nivelNavegacion){ //Barras agrupadas
    let grafico = nivelNavegacion == 'area' ? '#area-chart2-3' : '#chart2-3';
    let lineaEjeY = nivelNavegacion == 'area' ? 'area-expectativas-previsiones-ejeY' : 'slider--expectativas-previsiones-ejeY';

    //Creación de leyenda        
    let divLeyenda = document.createElement('div');
    divLeyenda.setAttribute('class','legend-three');

    let primerPunto = document.createElement('p');
    primerPunto.setAttribute('class','legend__item legend__item--expectativas-primero');
    primerPunto.textContent = 'III trim.';

    let segundoPunto = document.createElement('p');
    segundoPunto.setAttribute('class','legend__item legend__item--expectativas-segundo');
    segundoPunto.textContent = 'IV trim.';

    let tercerPunto = document.createElement('p');
    tercerPunto.setAttribute('class','legend__item legend__item--expectativas-tercero');
    tercerPunto.textContent = 'I trim. 2021';

    divLeyenda.appendChild(primerPunto);
    divLeyenda.appendChild(segundoPunto);
    divLeyenda.appendChild(tercerPunto);

    document.getElementById(grafico.substr(1)).appendChild(divLeyenda);
    
    //Estructura fundamental del gráfico
    let margin = {top: 20, right: 77.5, bottom: 20, left: 15},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom - (window.innerWidth < 425 ? 50 : 25);
    
    //Modificación del formato de datos
    let newData = [];
    for(let i = 0; i < data.length; i++){
        if(nivelNavegacion == 'area'){
            if(data[i].pais == 'P. Bajos' || data[i].pais == 'Austria' || data[i].pais == 'Grecia' || data[i].pais == 'Alemania' || data[i].pais == 'Eurozona' ||
                data[i].pais == 'Portugal' || data[i].pais == 'Italia' || data[i].pais == 'España' || data[i].pais == 'Francia'){
                    newData.push({categoria: data[i]['pais'], valores: [{descriptor:'I trim. 2021', valor: data[i].primer_trimestre}, 
                        {descriptor:'IV trim.', valor: data[i].cuarto_trimestre}, {descriptor:'III trim.', valor: data[i].tercer_trimestre} ]});
            }   
        } else {
            newData.push({categoria: data[i]['pais'], valores: [{descriptor:'I trim. 2021', valor: data[i].primer_trimestre}, 
                {descriptor:'IV trim.', valor: data[i].cuarto_trimestre}, {descriptor:'III trim.', valor: data[i].tercer_trimestre} ]});
        }             
    }

    let paises = newData.map(function(d) { return d.categoria });
    let descriptores = newData[0].valores.map(function(d) { return d.descriptor});

    //Estructura del gráfico
    let chart = d3.select(grafico)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje Y
    let y0 = d3.scaleBand()
        .rangeRound([height,0])
        .domain(paises)
        .padding(.5)
        .paddingOuter(.1);

    let y1 = d3.scaleBand()
        .range([y0.bandwidth(),0])
        .domain(descriptores);

    let yAxis = function(g){
        g.call(d3.axisRight(y0).tickSize(0))
        g.call(function(g){g.selectAll('.tick text')
            .style('fill', '#fff')
            .style('font-size', nivelNavegacion == 'area' ? '14.5px' : '13.5px')
            .style('font-family','Roboto')
            .style('font-weight','300')
            .style('opacity','1')})
        g.call(function(g){g.select('.domain').attr('id', lineaEjeY).style('opacity','0')});
    }        

    chart.append("g")        
        .attr('transform', 'translate(' + (width + 10) + ',0)')
        .call(yAxis);
    
    //Eje X
    let x = d3.scaleLinear()
        .domain([0.5,-12.5])
        .range([width,0.5]);

    let xAxis = function(g){
        g.call(d3.axisBottom(x).ticks(5))
        g.call(function(g){g.select('.domain').remove()})
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 0.5)
            .attr("stroke", "#343434")
            .attr("stroke-width", "1")
            .attr("y1", '0%')
            .attr("y2", '-' + (document.getElementById(lineaEjeY).getBoundingClientRect().height) + '')});
    }        

    chart.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);
    
    //Visualización de datos
    let slice = chart.selectAll(".slice")
      .data(newData)
      .enter().append("g")
      .attr("class", "g")
      .attr("transform",function(d) { return "translate(0," + y0(d.categoria) + ")"; });

    slice.selectAll("rect")
        .data(function(d) { return d.valores; })
        .enter()
        .append("rect")
        //.attr('width', function(d) { return x(0) - x(d.valor)})
        .attr("width", function(d) { return Math.abs(x(d.valor) - x(0)); })
        .attr('height', y1.bandwidth())
        .style('fill',  function(d){return d.descriptor == 'III trim.' ? '#661e1e' : d.descriptor == 'IV trim.' ? '#992d2d' : '#ff4a4a'})
        //.attr('x', function(d) { return x(d.valor)})
        .attr('x', function(d) { return d.valor > 0 ? x(0) : x(d.valor)})
        .attr('y', function(d) { return y1(d.descriptor)})
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipExpectativas.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipExpectativas.html('<span style="font-weight: 900;">' + d.valor.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipExpectativas.style('display','none').style('opacity','0');
        })
}

function getExpectativasConfianzaConsumidor(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart2-4' : '#chart2-4';

    //Creación de leyenda        
    let divLeyenda = document.createElement('div');
    divLeyenda.setAttribute('class','legend-three');

    let primerPunto = document.createElement('p');
    primerPunto.setAttribute('class','legend__item legend__item--expectativas-primero');
    primerPunto.textContent = 'Manufacturas';

    let segundoPunto = document.createElement('p');
    segundoPunto.setAttribute('class','legend__item legend__item--expectativas-tercero');
    segundoPunto.textContent = 'Servicios';

    let tercerPunto = document.createElement('p');
    tercerPunto.setAttribute('class','legend__item legend__item--expectativas-cuarto');
    tercerPunto.textContent = 'Total';

    divLeyenda.appendChild(primerPunto);
    divLeyenda.appendChild(segundoPunto);
    divLeyenda.appendChild(tercerPunto);

    document.getElementById(grafico.substr(1)).appendChild(divLeyenda);

    //Estructura fundamental del gráfico
    let margin = {top: 20, right: 2.5, bottom: 20, left: 25},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom - (window.innerWidth < 425 ? 50 : 25);

    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
    let x = d3.scaleBand()
        .domain(data.map(function(d) { return d.mes_v2 }))
        .range([0, width])
        .padding(0.1);
    
    let y = d3.scaleLinear()
        .range([height,0])
        .domain([0,60]);

    //Eje X
    let xAxis = function(g){
        g.call(d3.axisBottom(x).tickFormat(function(d) {return d.substr(-4)}))
        g.call(function(g){g.selectAll('.tick line').remove()})
        g.call(function(g){g.selectAll('.tick').attr('opacity', function(d,i) {return i == 0 || i == data.length - 1 ? '1' : '0'}).attr('text-anchor',function(d,i) {return i == 0 ? 'start' : i == data.length -1 ? 'end' : 'middle'})});
    }
        
    chart.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    //Eje Y
    let yAxis = function(g){
        g.call(d3.axisLeft(y).ticks(5))
        g.call(function(g){g.selectAll('.tick line').remove()})
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
    }        
    
    chart.append("g")
        .call(yAxis);

    // define the 1st line
    var valueline = d3.line()
        .x(function(d) { return x((d.mes_v2)); })
        .y(function(d) { return y(d.manufacturas); });

    // define the 2nd line
    var valueline2 = d3.line()
        .x(function(d) { return x(d.mes_v2); })
        .y(function(d) { return y(d.servicios); });

    var valueline3 = d3.line()
        .x(function(d) { return x(d.mes_v2); })
        .y(function(d) { return y(d.total_economia); });

    // Add the valueline path.
    chart.append("path")
        .data([data])
        .attr("fill", "none")
        .attr("stroke","#661e1e")
        .attr("stroke-width","1px")
        .attr("d", valueline);

    // Add the valueline2 path.
    chart.append("path")
        .data([data])
        .attr("fill", "none")
        .attr("stroke","#ff4a4a")
        .attr("stroke-width","1px")
        .attr("d", valueline2);

    // Add the valueline2 path.
    chart.append("path")
        .data([data])
        .attr("fill", "none")
        .attr("stroke","#ffc8c8")
        .attr("stroke-width","1px")
        .attr("d", valueline3);

    //Añadir círculos para las tres líneas
    //Manufacturas
    chart.selectAll('.circles')
        .data(data)
        .enter()
        .append('circle')
        .attr("r", 5)
        .attr("cx", function(d) { return x(d.mes_v2); })
        .attr("cy", function(d) { return y(d.manufacturas); })
        .style("fill", '#000')
        .style('opacity', '0')
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipExpectativas.transition()     
                .duration(200)
                .style('background-color', '#661e1e')
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipExpectativas.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><br><span style="font-weight: 900;">' + d.manufacturas.toFixed(1).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipExpectativas.style('display','none').style('opacity','0').style('background-color', '#ff4a4a');
        })

    //Servicios
    chart.selectAll('.circles')
        .data(data)
        .enter()
        .append('circle')
        .attr("r", 5)
        .attr("cx", function(d) { return x(d.mes_v2); })
        .attr("cy", function(d) { return y(d.servicios); })
        .style("fill", '#000')
        .style('opacity', '0')
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipExpectativas.transition()     
                .duration(200)
                .style('background-color', '#ff4a4a')
                .style('display', 'block')
                .style('opacity','0.9')
                tooltipExpectativas.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><br><span style="font-weight: 900;">' + d.servicios.toFixed(1).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipExpectativas.style('display','none').style('opacity','0').style('background-color', '#ff4a4a');
        })

    //Total economía
    chart.selectAll('.circles')
        .data(data)
        .enter()
        .append('circle')
        .attr("r", 5)
        .attr("cx", function(d) { return x(d.mes_v2); })
        .attr("cy", function(d) { return y(d.total_economia); })
        .style("fill", '#000')
        .style('opacity', '0')
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipExpectativas.transition()     
                .duration(200)
                .style('background-color', '#ffc8c8')
                .style('display', 'block')
                .style('opacity','0.9')
                tooltipExpectativas.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><br><span style="font-weight: 900;">' + d.total_economia.toFixed(1).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipExpectativas.style('display','none').style('opacity','0').style('background-color', '#ff4a4a');
        })    
}

function getExpectativasPMI(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart2-5' : '#chart2-5';   
    let lineaEjeX = nivelNavegacion == 'area' ? 'area-expectativas-europa-ejeX' : 'slider-expectativas-europa-ejeX';
    
    //Nos quedamos con los países que sí dispongan de datos
    let newData = data.filter(function(item) {
        if(!isNaN(item.confianza)){
            return item;
        }
    });

    function comparativa(a,b){
        const datoA = a.confianza;
        const datoB = b.confianza;

        let comparacion = 0;
        if (datoA > datoB) {
            comparacion = -1;
        } else if (datoA <= datoB) {
            comparacion = 1;
        }
        return comparacion;
    }

    newData = newData.sort(comparativa);
    
    //Estructura fundamental del gráfico
    let margin = {top: 10, right: 5, bottom: 90, left: 27.5},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;

    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleBand()
        .range([0,width])
        .domain(d3.range(newData.length))
        .paddingInner(.5)
        .align(1)

    let x2 = d3.scaleBand()
        .range([0,width])
        .domain(newData.map(function(d) { return d.pais }))
        .paddingInner(.5)
        .align(1)

    let xAxis = function(g){
        g.call(d3.axisBottom(x2))
        g.call(function(g){g.selectAll('.tick text').style('opacity','initial').attr('text-anchor','end').attr('transform', 'rotate(-90)').attr("dx", "-.8em").attr("dy", "0").attr("y", "3.5")})
        g.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {return d == 'España' ? '1' : '0.5'})})
        g.call(function(g){g.selectAll('.tick line').remove()})
        g.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')});
    }        

    chart.append('g')        
        .attr('transform', 'translate(0, ' + (height+10) + ')')
        .call(xAxis);
    
    //Eje Y
    let y = d3.scaleLinear()
        .domain([d3.min(newData, function(d) {return d.confianza}),d3.max(data, function(d) {return d.confianza})])
        .range([height,0])
        .nice();

    let yAxis = function(g){
        g.call(d3.axisLeft(y).ticks(6))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.select('.domain').remove()})
        g.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 0.5)
            .attr("stroke", function(d) {return d == 0 ? '#d9d9d9' : '#343434'})
            .attr("x1", '0%')
            .attr("x2", '' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')});
    }        

    chart.append('g')
        .call(yAxis);

    chart.selectAll('.barras')
        .data(newData)
        .enter()
        .append('rect')
        .attr('fill', "#ff4a4a")
        .attr('y', function(d) { return y(Math.max(0,d.confianza)) })
        .attr('x', function(d,i) { return x(i)})
        .attr('height', function(d) {return Math.abs(y(d.confianza) - y(0));})
        .attr('width', x.bandwidth())
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipExpectativas.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipExpectativas.html('<span style="font-weight: 900;">' + d.confianza.toFixed(1).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipExpectativas.style('display','none').style('opacity','0');
        })
}

//Cuentas Públicas
function getCuentasDeficitHistorico(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart3-1' : '#chart3-1';
    //Estructura fundamental del gráfico
    let margin = {top: 5, right: 7.5, bottom: 20, left: 25},
    width = (nivelNavegacion == 'area' ? (document.getElementById(grafico.substr(1)).parentElement.clientWidth) : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;
    
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleBand()
        .range([0,width])
        .domain(d3.range(data.length))
        .paddingInner(.75)
        .paddingOuter(.25)
        .align(.5);

    let x2 = d3.scaleTime()
        .domain(d3.extent(data, function(d) {return formatMonthTime(d.mes)}))
        .range([0, width]);
    
    let xAxis = function(g){
        g.call(d3.axisBottom(x).tickFormat(function(d){if(d == 0){return '2004'} else if (d == data.length - 1){return '2020'}}))
        g.call(function(g){g.selectAll('.tick line').remove()})
        g.call(function(g){g.select('.domain').attr('id','domain-cuentas-deficit-' + nivelNavegacion)})
        //g.call(function(g){g.selectAll('.tick').attr('text-anchor', function(d,i) {return i == 0 ? 'start' :  'end'})});
    }        
    
    chart.append('g')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);

    //Eje Y
    let y = d3.scaleLinear()
        .domain([-4.5,1])
        .range([height,0]);

    let yAxis = function(g){
        g.call(d3.axisLeft(y).ticks(6))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d){ return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.select('.domain').remove()})
        g.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 0.5)
            .attr("stroke", "#343434")
            .attr("x1", '0%')
            .attr("x2", '' + (document.getElementById('domain-cuentas-deficit-' + nivelNavegacion).getBoundingClientRect().width) + '')});
    }        
    
    chart.append('g')
        .call(yAxis);

    //Visualización
    chart.selectAll('.barras')
        .data(data)
        .enter()
        .append('rect')
        .attr('fill', '#ff9574')
        .attr('y', function(d) { return y(Math.max(0,d.deficit)) })
        .attr('x', function(d,i) { return x(i)})
        .attr('height', function(d) {return Math.abs(y(d.deficit) - y(0));})
        .attr('width', x.bandwidth())
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipCuentas.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipCuentas.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes) + '</span><br><span style="font-weight: 900;">' + d.deficit.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipCuentas.style('display','none').style('opacity','0');
        })    
}

function getCuentasDeudaPublica(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart3-2' : '#chart3-2';
    let lineaEjeX = nivelNavegacion == 'area' ? 'area-cuentas-deuda-ejeX' : 'slider-cuentas-deuda-ejeX';

    //Estructura fundamental del gráfico
    let margin = {top: 0, right: 7.5, bottom: 20, left: 30},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;
    
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleBand()
        .range([0,width])
        .domain(d3.range(data.length));

    let x2 = d3.scaleTime()
        .range([0,width])
        .domain(d3.extent(data, function(d) { return formatTime(d.trimestre, "cuentas_publicas") }));

    let xAxis = function(g){
        g.call(d3.axisBottom(x2))
        g.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')})
        g.call(function(g){g.selectAll('.tick line').remove()})
        g.call(function(g){g.selectAll('.tick').attr('opacity', function(d){return d.getFullYear() == 2020 || d.getFullYear() == 1996 || d.getFullYear() == 2004 || d.getFullYear() == 2012 ? '1' : '0'})})
        g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d){return d.getFullYear() == 2020 ? 'end' : 'middle'})})
        
    }        

    chart.append('g')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);
    
    //Eje Y
    let y0 = Math.max(Math.abs(d3.min(data, function(d) {return d.porcentaje_deuda})), Math.abs(d3.max(data, function(d) {return d.porcentaje_deuda})));

    let y = d3.scaleLinear()
        .domain([0,y0])
        .range([height,0])
        .nice();

    let yAxis = function(g){
        g.call(d3.axisLeft(y).ticks(6))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d){ return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.select('.domain').remove()})
        g.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 0.5)
            .attr("stroke", "#343434")
            .attr("x1", '0%')
            .attr("x2", '' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')});
    }        

    chart.append('g')
        .call(yAxis);

    //Visualización
    chart.selectAll('.barras')
        .data(data)
        .enter()
        .append('rect')
        .attr('fill', '#ff9574')
        .attr('y', function(d) { return y(Math.max(0,d.porcentaje_deuda)) })
        .attr('x', function(d,i) { return x(i)})
        .attr('height', function(d) {return Math.abs(y(d.porcentaje_deuda) - y(0));})
        .attr('width', x.bandwidth() / 2)
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipCuentas.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipCuentas.html('<span style="font-weight: 300;">' + formatTooltipDate(d.trimestre) + '</span><br><span style="font-weight: 900;">' + d.porcentaje_deuda.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipCuentas.style('display','none').style('opacity','0');
        })
}

function getCuentasRecaudacion(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart3-3' : '#chart3-3';
    let lineaEjeY = nivelNavegacion == 'area' ? 'area-cuentasRecaudacionY' : 'slider-cuentasRecaudacionY';

    // //Crear botones para mapa o gráfico de barras
    let grafico2 = document.getElementById(grafico.substring(1));
    let grupoBotones = document.createElement('div');
    grupoBotones.classList.add('chart-buttons');

    let firstButton = document.createElement('button');
    firstButton.textContent = 'Porcentaje';
    firstButton.classList.add('button');
    firstButton.classList.add('button__cuentas');
    firstButton.classList.add('button-active');
    let secondButton = document.createElement('button');
    secondButton.textContent = 'Millones';
    secondButton.classList.add('button');
    secondButton.classList.add('button__cuentas');
    let thirdButton = document.createElement('button');
    thirdButton.textContent = 'Acumulada';
    thirdButton.classList.add('button');
    thirdButton.classList.add('button__cuentas');

    grupoBotones.appendChild(firstButton);
    grupoBotones.appendChild(secondButton);
    grupoBotones.appendChild(thirdButton);
    grafico2.insertBefore(grupoBotones, grafico2.firstChild);

    //Asociamos los botones a un evento que cambiará la forma de datos
    let tipo = 'porcentaje';

    firstButton.addEventListener('click', function() {
        updateChart('porcentaje')
        firstButton.classList.add('button-active');
        secondButton.classList.remove('button-active');
        thirdButton.classList.remove('button-active');
        getOutTooltips();
    });
    secondButton.addEventListener('click', function() {
        updateChart('millones');
        secondButton.classList.add('button-active');
        firstButton.classList.remove('button-active');
        thirdButton.classList.remove('button-active');
        getOutTooltips();
    });
    thirdButton.addEventListener('click', function() {
        updateChart('acumulada');
        secondButton.classList.remove('button-active');
        firstButton.classList.remove('button-active');
        thirdButton.classList.add('button-active');
        getOutTooltips();
    });

    //Estructura fundamental del gráfico
    let alturaGrafico = 57.5;

    //Estructura fundamental del gráfico
    let margin = {top: 20, right: 10, bottom: 20, left: 145},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom - alturaGrafico;

    // append the svg object to the body of the page
    let chart = d3.select(grafico)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje Y
    let y = d3.scaleBand()
        .range([0, height])
        .domain(data.map(function(d) { return d.figura_mostrar; }));

    let yAxis = function(g){
        g.call(d3.axisLeft(y))
        g.call(function(g){g.selectAll('.domain').attr('id', lineaEjeY).style('opacity','0')})
        g.call(function(g){g.selectAll('.tick text').style('font-size', nivelNavegacion == 'area' ? '14px' : '15px').style('opacity','1').style('font-weight','300')})
        g.call(function(g){g.selectAll('.tick line').remove()});
    }        

    chart.append("g")
        .attr('class', 'y-axis')
        .call(yAxis);
    
    //Eje X
    let x = d3.scaleLinear()
        .range([20, width]);

    let xAxis = function(g){
        g.call(d3.axisBottom(x).ticks(3))
        g.attr("transform", "translate(0," + (height) + ")")
        g.call(function(g){g.select('.domain').remove()})
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.selectAll('.tick line')
            .attr('y1','0')
            .attr('y2', '-' + (document.getElementById(lineaEjeY).getBoundingClientRect().height) + '')
            .style('stroke','#343434')
            .style('stroke-dasharray','.8')})
    }

    function createChart(){
        //X axis
        x.domain([-100, 50]);

        chart.append("g")
            .attr('class', 'x-axis')
            .call(xAxis);

        // Lines
        chart.selectAll("myline")
        .data(data)
        .enter()
        .append("line")
            .attr('class','barras')
            .attr("x1", function(d) { return x(0); })
            .attr("x2", function(d) { return x(d.variacion_porc); })
            .attr("y1", function(d) { return y(d.figura_mostrar) + (y.bandwidth() / 2); })
            .attr("y2", function(d) { return y(d.figura_mostrar) + (y.bandwidth() / 2); })
            .attr("stroke", "#ff9574")
            .attr("stroke-width", '4px')
            .on('touchstart mouseover', function(d) {
                //Dato a mostrar
                if(tipo == 'porcentaje'){
                    dato = d.variacion_porc.toFixed(2).replace('.',',') + '%';
                } else if (tipo == 'acumulada'){
                    dato = d.variacion_acumulada_porc.toFixed(2).replace('.',',') + '%';
                } else {
                    dato = d.variacion_millones;
                }

                //Disposición del tooltip
                let bodyWidth = parseInt(d3.select('body').style('width'));
                let mapHeight = parseInt(d3.select('body').style('height'));
        
                // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
                // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
        
                tooltipCuentas.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipCuentas.html('<span style="font-weight: 900;">' + dato + '</span>')
                    .style("left", ""+ (d3.event.pageX - 25) +"px")     
                    .style("top", ""+ (d3.event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipCuentas.style('display','none').style('opacity','0');
            })
    }

    function updateChart(type){        
        if(type == 'porcentaje'){
            x.domain([-100,50])
        } else if ( type == 'acumulada'){
            x.domain([-20,10])
        } else {
            x.domain(d3.extent(data, function(d) { return d.variacion_millones }))
        }
        tipo = type;
        
        let chart = d3.select(grafico).transition();

        chart.select('.x-axis')
            .duration(750)
            .call(xAxis);

        chart.selectAll('.barras')
            .duration(750)
            .attr("x1", function(d) { return x(0); })
            .attr("x2", function(d) {
                if(type == 'porcentaje'){
                    return x(d.variacion_porc); 
                } else if (type == 'acumulada'){
                    return !isNaN(d.variacion_acumulada_porc) ? x(d.variacion_acumulada_porc) : x(0);                 
                } else {
                    return x(d.variacion_millones); 
                }                
            });
    }

    //Por defecto, se crea con porcentaje
    createChart();
}

function getCuentasDeficitCCAA(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart3-4' : '#chart3-4';

    //Creación de leyenda        
    let divLeyenda = document.createElement('div');
    divLeyenda.setAttribute('class','legend');

    let primerPunto = document.createElement('p');
    primerPunto.setAttribute('class','legend__item legend__item--cuentas-primero');
    primerPunto.textContent = 'Menos de -0,5';

    let segundoPunto = document.createElement('p');
    segundoPunto.setAttribute('class','legend__item legend__item--cuentas-segundo');
    segundoPunto.textContent = '-0,5 a 0';

    let tercerPunto = document.createElement('p');
    tercerPunto.setAttribute('class','legend__item legend__item--cuentas-tercero');
    tercerPunto.textContent = '-0 a 0,5';

    let cuartoPunto = document.createElement('p');
    cuartoPunto.setAttribute('class','legend__item legend__item--cuentas-cuarto');
    cuartoPunto.textContent = 'Más de 0,5';    

    divLeyenda.appendChild(primerPunto);
    divLeyenda.appendChild(segundoPunto);
    divLeyenda.appendChild(tercerPunto);
    divLeyenda.appendChild(cuartoPunto);

    document.getElementById(grafico.substr(1)).appendChild(divLeyenda);

    d3.json(mapaAutonomias, function(error, mapa) {
        if (error) throw error;
        //No hay datos para Ceuta y Melilla, por lo que no es necesario recuperarlas
        let coordenadasMapa = topojson.feature(mapa, mapa.objects.autonomous_regions);
        let datosMapa = coordenadasMapa.features;

        data.forEach(function(item){
            datosMapa.forEach(function(item2){ //Sólo quedan sin datos Ceuta y Melilla
                if(parseInt(item.id_ccaa) == parseInt(item2.id)){
                    item2.ccaa = item.ccaa;
                    item2.deficit_acumulado = item.deficit_acumulado;
                }
            })
        });    
        
        //Estructura fundamental del gráfico
        let margin = {top: 20, right: 20, bottom: 20, left: 30},
        width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth),
        height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - (window.innerWidth < 425 ? 50 : 25);
            
        //Inicialización del gráfico
        let chart = d3.select(grafico)
            .append('svg')
            .attr('width', width)
            .attr('height', height);

        const projection = d3.geoConicConformalSpain().fitSize([width,height], coordenadasMapa);
        const path = d3.geoPath(projection);

        chart.selectAll(".region")
            .data(datosMapa)
            .enter()
            .append("path")
            .attr('id', function(d){return d.ccaa})
            .attr("d", path)
            .style("fill", function(d){return d.deficit_acumulado < -0.5 ? '#3d1f17' : d.deficit_acumulado < 0 && d.deficit_acumulado >= -0.5 ? '#844734' : d.deficit_acumulado < 0.5 && d.deficit_acumulado >= 0 ? '#ff9574' : '#ffd4c7'})
            .style("stroke", "#000")
            .style("stroke-width", ".6px")
            .on('touchstart mouseover', function(d){
                let bodyWidth = parseInt(d3.select('body').style('width'));
                let mapHeight = parseInt(d3.select('body').style('height'));
        
                // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
                // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
        
                tooltipCuentas.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipCuentas.html('<span style="font-weight: 300;">' + d.ccaa + '</span><br><span style="font-weight: 900;">' + d.deficit_acumulado.toFixed(2).replace('.',',') + '%</span>')
                    .style("left", ""+ (d3.event.pageX - 25) +"px")     
                    .style("top", ""+ (d3.event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d){
                tooltipCuentas.style('display','none').style('opacity','0');
            })
    });

    // d3.json(mapaAutonomias).then(function(mapa){
        
    // }).catch(function(error) {console.log(error)});
}

//Empleo
function getEmpleoAfiliacionProvincias(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart4-1' : '#chart4-1';
    let anchoGraficos = 95;

    if ( nivelNavegacion == 'area' ) {
        document.getElementById('area-chart4-1').classList.add('graph__content--empleo');
        let anchoPadre = document.getElementById('area-chart4-1').clientWidth;
        anchoGraficos = (anchoPadre / 3) - 40;
    }

    let margin = {top: 15, right: 10, bottom: 25, left: 30},
    width = anchoGraficos,
    height = 97.25;

    let formatData = formatTimeData(data);

    if(nivelNavegacion == 'area'){
        formatData = formatData.filter(function(item){
            if(item.provincia == 'BALEARES' || item.provincia == 'HUELVA' || item.provincia == 'TENERIFE' || item.provincia == 'LAS PALMAS' || item.provincia == 'CEUTA' || item.provincia == 'CÁCERES'){
                return item;
            }
        })
    }

    let provincias = d3.nest()
        .key(function(d) {return d.provincia;})
        .entries(formatData);

    let fechas = d3.nest()
        .key(function(d) { return d.fecha})
        .entries(formatData);

    let xScale = d3.scaleBand()
        .domain(fechas.map(function(d) {return d.key}))
        .align(1)
        .rangeRound([0, width]);

    let yScale = d3.scaleLinear()
        .domain([75,d3.max(formatData, function(d) { return d.dato + 15; })])
        .range([height, 10]);

    let xAxis = function(g){
        g.call(d3.axisBottom().scale(xScale).tickFormat(function(d) {if(d == 'Febrero'){return 'feb.'}else if(d == 'Junio'){return 'jun.'}}))
        g.call(function(g){g.selectAll('line').remove()});
    }          

    let yAxis = function(g){
        g.call(d3.axisLeft().scale(yScale).ticks(3))
        g.call(function(g){g.selectAll('.tick line').remove()});
    }        

    let line = d3.line()
        .x(function(d) { return xScale(d.fecha) + 10; })
        .y(function(d) { return yScale(d.dato); });

    let chart = d3.select(grafico)
        .selectAll('prueba-interior')
        .data(provincias)
        .enter()
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    chart.append("g").call(yAxis);
    chart.append("g").attr('transform', 'translate(0, ' + height + ')').call(xAxis);
    
    chart
        .append("text")
        .attr("text-anchor", "middle")
        .attr("y", 0)
        .attr("x", width/2)
        .text(function(d){
            let palabra = d.key;
            let possibleSplit = palabra.split(" ");
            let resultado = '';
            if(possibleSplit.length > 1){
                let primeraLetra = possibleSplit[0].charAt(0);
                let restoPrimera = possibleSplit[0].slice(1);
                let segundaLetra = possibleSplit[1].charAt(0);
                let restoSegunda = possibleSplit[1].slice(1);
                resultado = primeraLetra.toUpperCase() + restoPrimera.toLowerCase() + " " + segundaLetra.toUpperCase() + restoSegunda.toLowerCase();
            } else {
                let primeraLetra = palabra.charAt(0);
                let resto = palabra.slice(1);
                resultado = primeraLetra.toUpperCase() + resto.toLowerCase();
            }
            return resultado;
        })
        .style('fill', '#fff')
        .style('font-size','14.5px')
        .style('font-weight','300')
        .style('font-family','Roboto')
        .style('color','#fff')

    // // Add the line path elements
    chart.append("path")
        .attr("class", "line")
        .attr("stroke", "#ffe68c")
        .attr("stroke-width", "1px")
        .attr("d", function(d) { return line(d.values); });

    chart.selectAll("circle")
        .data(function(d){return d.values})
        .enter()
        .append("circle")
        .attr("r", 3)
        .attr("cx", function(d) { return xScale(d.fecha) + 10; })
        .attr("cy", function(d) { return yScale(d.dato); })
        .style("fill", "#ffe68c")
        .style("opacity", function(d,i) {if(i == 0 || i == provincias[0].values.length - 1){ return 1 } else { return 0 }})
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipEmpleo.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipEmpleo.html('<span style="font-weight: 300;">' + d.fecha + '</span><br><span style="font-weight: 900;">' + d.dato.toFixed(2).replace('.',',') + '</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipEmpleo.style('display','none').style('opacity','0');
        }) 
}

function getEmpleoAfiliacionHistorico(data, nivelNavegacion){    
    let grafico = nivelNavegacion == 'area' ? '#area-chart4-2' : '#chart4-2';
    let lineaEjeX = nivelNavegacion == 'area' ? 'area-empleo-afiliacion-historico-ejeX' : 'slider-empleo-afiliacion-historico-ejeX';
    
    //Estructura fundamental del gráfico
    let margin = {top: 5, right: 5, bottom: 20, left: 45},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;

    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleTime()
        .domain(d3.extent(data, function(d) {return formatDayTime(d.fecha)}))
        .range([0, width]);
    
    let xAxis = function(g){
        g.call(d3.axisBottom(x).tickValues(x.domain().filter(function(d,i) { return !(i%1) })).tickFormat(function(d){ return meses[d.getMonth()];}))
        g.call(function(g){g.selectAll('.tick line').remove()})
        g.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','1')})
        g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d,i) {return i == 0 ? 'start' : 'end'})});
    }

    chart.append('g')        
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);

    //Eje Y
    let y = d3.scaleLinear()
        //.domain(d3.extent(data, function(d) {return d.afiliados}))
        .domain([d3.min(data, function(d){ return d.afiliados - 100000 }),19500000])
        .range([height,0]);

    let yAxis = function(g){
        g.call(d3.axisLeft(y).ticks(4).tickFormat(d3.format(".3s")))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.selectAll('.tick line')
            .attr('x2','0')
            .attr('x1','' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')
            .style('stroke','#343434')
            .style('stroke-width','1')
            .style('opacity', function(d) {return d == 0 ? '1' : '0'})});
    }
    
    chart.append("g")
        .attr('transform', 'translate(0, 0)')
        .call(yAxis);

    //Line
    let line = d3.line()
        .x(function(d){ return x(formatDayTime(d.fecha))})
        .y(function(d){return y(d.afiliados)});
    
    chart.append("path")
        .datum(data)
        .attr("fill", "none")
        .attr("stroke", "#ffe68c")
        .attr("stroke-width", 0.8)
        .attr('transform', 'translate(0, 0)')
        .attr("d", line);

    //Area > set the gradient
    chart.append("linearGradient")				
    .attr("id", nivelNavegacion == 'area' ? 'area-empleo-afiliacion' : 'slider-empleo-afiliacion')			
    .attr("gradientUnits", "userSpaceOnUse")	
    .attr("x1", 0).attr("y1", y(d3.min(data, function(d) {return d.afiliados})))			
    .attr("x2", 0).attr("y2", y(d3.max(data, function(d) {return d.afiliados})))		
    .selectAll("stop")						
    .data([
        {offset: "0%", color: "rgba(255, 230, 140, 0)"},
        {offset: "70%", color: "rgba(255, 230, 140, 0.2)"},
        {offset: "100%", color: "rgba(255, 230, 140, 0.45)"}	
    ])					
    .enter()
    .append("stop")			
    .attr("offset", function(d) { return d.offset; })	
    .attr("stop-color", function(d) { return d.color; });

    let area = d3.area()
        .x(function(d) { return x(formatDayTime(d.fecha)) })	
        .y0(height)					
        .y1(function(d) { return y(d.afiliados); });

    chart.append('path')
        .datum(data)
        .attr('fill', nivelNavegacion == 'area' ? 'url(#area-empleo-afiliacion)' : 'url(#slider-empleo-afiliacion)')
        .attr('d', area);

    //Dibujo círculos
    chart.selectAll('.circles')
        .data(data)
        .enter()
        .append('circle')
        .attr("r", 5)
        .attr("cx", function(d) { return x(formatDayTime(d.fecha)) })
        .attr("cy", function(d) { return y(d.afiliados); })
        .style("fill", '#000')
        .style('opacity', '0')
        .attr('transform', 'translate(2.5, 0)')
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipEmpleo.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipEmpleo.html('<span style="font-weight: 300;">' + formatTooltipDate(d.fecha) + '</span><br><span style="font-weight: 900;">' + getNumberWithCommas(d.afiliados) + '</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipEmpleo.style('display','none').style('opacity','0');
        })
}

function getEmpleoTasaParoHistorica(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart4-3' : '#chart4-3';
    let lineaEjeX = nivelNavegacion == 'area' ? 'area-empleo-tasa-paro-historica-ejeX' : 'slider-empleo-tasa-paro-historica-ejeX';

    //Estructura fundamental del gráfico
    let margin = {top: 20, right: 5, bottom: 20, left: 30},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;

    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleBand()
        .range([0,width])
        .domain(d3.range(data.length));

    let x2 = d3.scaleTime()
        .range([0,width])
        .domain(d3.extent(data, function(d) { return formatTime(d.trimestre_v2) }));

    let xAxis = function(g){
        g.call(d3.axisBottom(x2).ticks(18).tickFormat(function(d) { return d.getFullYear(); }))
        g.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {if(d.getFullYear() == 2002 || d.getFullYear() == 2008 || d.getFullYear() == 2013 ||d.getFullYear() == 2020){return 1} else {return 0}})})
        g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d) {return d.getFullYear() == 2020 ? 'end' : d.getFullYear() == 2002 ? 'start' : 'middle' })})
        g.call(function(g){g.selectAll('line').remove()})
        g.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')});
    }        

    chart.append('g')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);
    
    //Eje Y
    let y0 = Math.max(Math.abs(d3.min(data, function(d) {return d.tasa_paro})), Math.abs(d3.max(data, function(d) {return d.tasa_paro})));

    let y = d3.scaleLinear()
        .domain([0,30])
        .range([height,0])
        .nice();

    let yAxis = function(g){
        g.call(d3.axisLeft(y).ticks(7))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.select('.domain').remove()})
        g.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 0.5)
            .attr("stroke", function(d) {return d == 0 ? '#d9d9d9' : '#343434'})
            .attr("x1", '0%')
            .attr("x2", '' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')});
    }        

    chart.append('g')
        .attr("transform", 'translate(0,0)')
        .call(yAxis);

    chart.selectAll('.barras')
        .data(data)
        .enter()
        .append('rect')
        .attr('fill', '#ffe68c')
        .attr('y', function(d) { return y(Math.max(0,d.tasa_paro)) })
        .attr('x', function(d,i) { return x(i)})
        .attr('height', function(d) {return Math.abs(y(d.tasa_paro) - y(0));})
        .attr('width', x.bandwidth() / 2)
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipEmpleo.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipEmpleo.html('<span style="font-weight: 300;">' + d.trimestre + '</span><br><span style="font-weight: 900;">' + d.tasa_paro.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipEmpleo.style('display','none').style('opacity','0');
        })
}

function getEmpleoNumeroParados(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart4-4' : '#chart4-4';

    //Estructura fundamental del gráfico
    let margin = {top: 20, right: 5, bottom: 20, left: 30},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;
    
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleTime()
        .domain(d3.extent(data, function(d) {return formatTime(d.mes_v2, "mes en letras")}))
        .range([0, width]);
    
    let xAxis = function(g){
        g.call(d3.axisBottom(x))
        g.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {if(d.getFullYear() == 1996 || d.getFullYear() == 2004 || d.getFullYear() == 2012 || d.getFullYear() == 2020){return 1} else {return 0}})})
        g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d) {return d.getFullYear() == 2020 ? 'end' : d.getFullYear() == 1996 ? 'start' : 'middle' })})
        g.call(function(g){g.selectAll('.tick line').remove()});
    }
    
    chart.append('g')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);

    //Eje Y
    let y = d3.scaleLinear()
        .domain([0,d3.max(data, function(d) {return d.paro_registrado})])
        .range([height,0]);

    let yAxis = function(g){
        g.attr("transform", 'translate(0,0)')
        g.call(d3.axisLeft(y).ticks(5).tickFormat(d3.format(".0s")))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.selectAll('.tick line').remove()});}
    
    chart.append("g")
        .attr('transform', 'translate(0, 0)')
        .call(yAxis);

    //Line
    let line = d3.line()
        .x(function(d){return x(formatTime(d.mes_v2, "mes en letras"))})
        .y(function(d){return y(d.paro_registrado)});
    
    chart.append("path")
        .datum(data.filter(line.defined()))
        .attr("fill", "none")
        .attr("stroke", "#ffe68c")
        .attr("stroke-width", 1)
        .attr('transform', 'translate(0, 0)')
        .attr("d", line);

    //Dibujo círculos
    chart.selectAll('.circles')
        .data(data)
        .enter()
        .append('circle')
        .attr("r", 5)
        .attr("cx", function(d) { return x(formatTime(d.mes_v2, "mes en letras")); })
        .attr("cy", function(d) { return y(d.paro_registrado); })
        .style("fill", '#000')
        .style('opacity', '0')
        .attr('transform', 'translate(2.5, 0)')
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipEmpleo.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipEmpleo.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><br><span style="font-weight: 900;">' + getNumberWithCommas(d.paro_registrado.toFixed(2).replace('.',',')) + '</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipEmpleo.style('display','none').style('opacity','0');
        })
}

function getEmpleoCaidaEmpleo(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart4-5' : '#chart4-5';
    let lineaEjeX = nivelNavegacion == 'area' ? 'area-caida-empleo-ue-x' : 'slide-caida-empleo-ue-x';

    //Estructura fundamental del gráfico
    let margin = {top: 10, right: 5, bottom: 20, left: 95},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;
    
    let newData = [];

    if (nivelNavegacion == 'area'){
        for(let i = 0; i < data.length; i++){
            if(data[i].pais == 'UE' || data[i].pais == 'Eurozona' || data[i].pais == 'Alemania' || data[i].pais == 'Grecia' || data[i].pais == 'España' ||
            data[i].pais == 'Francia' || data[i].pais == 'Italia' || data[i].pais == 'P. Bajos' || data[i].pais == 'Portugal' || data[i].pais == 'Reino Unido'){
                newData.push(data[i]);
            }
        }
    } else {
        newData = data.slice();
    }

    // append the svg object to the body of the page
    let svg = d3.select(grafico)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //X axis
    let x = d3.scaleLinear()
    .domain([-10, 5])
    .range([0, width]);

    let xAxis = function(svg){
        svg.call(d3.axisBottom(x).ticks(4))
        svg.call(function(g){g.selectAll('.domain').attr('id', lineaEjeX).style('opacity','0')})
        svg.call(function(g){g.selectAll('.tick line').remove()})
        svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        svg.attr("transform", "translate(0," + (height - 2.5) + ")");
    }        

    svg.append("g")
        .call(xAxis);

    // Y axis
    let y = d3.scaleBand()
    .range([0, height])
    .domain(newData.map(function(d) { return d.pais; }))
    .paddingOuter(0);

    let yAxis = function(svg){
        svg.call(d3.axisLeft(y))
        svg.call(function(g){g.selectAll('.domain').remove()})
        svg.call(function(g){g.selectAll('.tick text').style('font-size', nivelNavegacion == 'area' ? '15px' : '14px').style('opacity','1').style('font-weight','300')})
        svg.call(function(g){g.selectAll('.tick line')
            .attr('x2','0')
            .attr('x1','' + document.getElementById(lineaEjeX).getBoundingClientRect().width + '')
            .style('stroke','#343434')
            .style('stroke-dasharray','2')});
    }        

    svg.append("g")
    .call(yAxis);

    // Lines
    svg.selectAll("myline")
    .data(newData)
    .enter()
    .append("line")
        .attr("x1", function(d) { return x(0); })
        .attr("x2", function(d) { return x(d.variacion_empleados); })
        .attr("y1", function(d) { return y(d.pais) + (y.bandwidth() / 2); })
        .attr("y2", function(d) { return y(d.pais) + (y.bandwidth() / 2); })
        .attr("stroke", "lightgrey")
        .attr("stroke-width", "1px")

    // Circles of variable 1
    svg.selectAll("mycircle")
    .data(newData)
    .enter()
    .append("circle")
        .attr("cx", function(d) { return x(0); })
        .attr("cy", function(d) { return y(d.pais) + (y.bandwidth() / 2); })
        .attr("r", "3")
        .style("fill", "#fdf4d2")

    //Circles of 2
    svg.selectAll("mycircle")
    .data(newData)
    .enter()
    .append("circle")
    .attr("cx", function(d) { return x(d.variacion_empleados); })
    .attr("cy", function(d) { return y(d.pais) + (y.bandwidth() / 2); })
    .attr("r", "3")
    .style("fill", "#ffe68c")
    .on('touchstart mouseover', function(d) {
        let bodyWidth = parseInt(d3.select('body').style('width'));
        let mapHeight = parseInt(d3.select('body').style('height'));

        tooltipEmpleo.transition()     
            .duration(200)
            .style('display', 'block')
            .style('opacity','0.9')
        tooltipEmpleo.html('<span style="font-weight: 900;">' + getNumberWithCommas(d.variacion_empleados.toFixed(2).replace('.',',')) + '%</span>')
            .style("left", ""+ (d3.event.pageX - 25) +"px")     
            .style("top", ""+ (d3.event.pageY - 35) +"px");
    })
    .on('touchend mouseout mouseleave', function(d) {
        tooltipEmpleo.style('display','none').style('opacity','0');
    });
}

function getEmpleoTasaParoEuropa(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart4-6' : '#chart4-6';
    let lineaEjeX = nivelNavegacion == 'area' ? 'area-empleo-tasa-paro-europa-ejeX' : 'slider-empleo-tasa-paro-europa-ejeX';

    //Nos quedamos con los países que sí dispongan de datos
    let newData = data.filter(function(item) {
        if(!isNaN(item.datos_paro)){
            return item;
        }
    });

    function comparativa(a,b){
        const datoA = a.datos_paro;
        const datoB = b.datos_paro;

        let comparacion = 0;
        if (datoA > datoB) {
            comparacion = 1;
        } else if (datoA <= datoB) {
            comparacion = -1;
        }
        return comparacion;
    }

    newData = newData.sort(comparativa);    

    //Estructura fundamental del gráfico
    let margin = {top: 20, right: 10, bottom: 45, left: 25},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;

    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleBand()
        .domain(newData.map(function(d){return d.pais}))
        .range([0, width])
        .paddingInner(.5)
        .align(1)

    let xAxis = function(g){
        g.call(d3.axisBottom(x))
        g.call(function(g){g.selectAll('.tick text').style('font-size', nivelNavegacion == 'area' ? '12px' : '13px').style('opacity','initial').attr('text-anchor','end').attr('transform', 'rotate(-90)').attr("dx", "-.8em").attr("dy", "0").attr("y", "3.5")})
        g.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {return d == 'España' ? '1' : '0.5'})})
        g.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')});
    }        

    chart.append('g')
        .attr("transform", "translate(0," + (height - 50) + ")")
        .call(xAxis);

    //Eje Y
    let y = d3.scaleLinear()
        .range([height - 57.5,0])
        .domain([0,20]);

    let yAxis = function(g){
        g.call(d3.axisLeft(y).ticks(5))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.select('.domain').remove()})
        g.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 0.5)
            .attr("stroke", function(d) {return d == 0 ? '#d9d9d9' : '#343434'})
            .attr("x1", '0%')
            .attr("x2", '' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')});
    }        
    
    chart.append('g')
        .attr('transform', 'translate(0,0)')
        .call(yAxis);

    //Creación de barras
    chart.selectAll("bar")
        .data(newData)
        .enter()
        .append("rect")
        .style("fill", "#ffe68c")
        .attr("x", function(d) { return x(d.pais); })
        .attr("width", x.bandwidth())
        .attr("y", function(d) {return !isNaN(d.datos_paro) ? y(d.datos_paro) : y(0); })
        .attr("height", function(d) { return (height - 57.5) - y(d.datos_paro); })
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipEmpleo.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipEmpleo.html('<span style="font-weight: 900;">' + d.datos_paro.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipEmpleo.style('display','none').style('opacity','0');
        })
}

//Demanda
function getDemandaHistorica(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart5-1' : '#chart5-1';

    //Estructura fundamental del gráfico
    let margin = {top: 5, right: 5, bottom: 20, left: 30},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;
    
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleTime()
        .domain(d3.extent(data, function(d) {return formatTime(d.mes_v2, "mes en letras")}))
        .range([0, width]);
    
    let xAxis = function(g){
        g.call(d3.axisBottom(x))
        g.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {if(d.getFullYear() == 2000 || d.getFullYear() == 2006 || d.getFullYear() == 2014 || d.getFullYear() == 2020){return 1} else {return 0}})})
        g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d) {return d.getFullYear() == 2020 ? 'end' : d.getFullYear() == 2000 ? 'start' : 'middle' })})
        g.call(function(g){g.selectAll('.tick line').remove()});
    }        
    
    chart.append('g')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);

    //Eje Y
    let y = d3.scaleLinear()
        //.domain([0,d3.max(data, (d) => {return d.indice_precios_corrientes})])
        .domain([0,125])
        .range([height,0]);

    let yAxis = function(g){
        g.attr("transform", 'translate(0,0)')
        g.call(d3.axisLeft(y).ticks(6))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.selectAll('.tick line').remove()});
    }        
    
    chart.append("g")
        .attr('transform', 'translate(0, 0)')
        .call(yAxis);

    //Line
    let line = d3.line()
        .x(function(d){return x(formatTime(d.mes_v2, "mes en letras"))})
        .y(function(d){return y(d.indice_precios_corrientes)});
    
    chart.append("path")
        .datum(data.filter(line.defined()))
        .attr("fill", "none")
        .attr("stroke", "#59fba5")
        .attr("stroke-width", 1)
        .attr('transform', 'translate(0, 0)')
        .attr("d", line);

    //Dibujo círculos
    chart.selectAll('.circles')
        .data(data)
        .enter()
        .append('circle')
        .attr("r", 5)
        .attr("cx", function(d) { return x(formatTime(d.mes_v2, "mes en letras")); })
        .attr("cy", function(d) { return y(d.indice_precios_corrientes); })
        .style("fill", '#000')
        .style('opacity', '0')
        .attr('transform', 'translate(2.5, 0)')
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipDemanda.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipDemanda.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><br><span style="font-weight: 900;">' + d.indice_precios_corrientes.toFixed(2).replace('.',',') + '</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipDemanda.style('display','none').style('opacity','0');
        })
}

function getDemandaSectorial(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart5-2' : '#chart5-2';
    let lineaEjeY = nivelNavegacion == 'area' ? 'area-demanda-sectorial-ejeY' : 'slider-demanda-sectorial-ejeY';

    //Creación de leyenda        
    let divLeyenda = document.createElement('div');
    divLeyenda.setAttribute('class','legend legend-five-items');

    let primerPunto = document.createElement('p');
    primerPunto.setAttribute('class','legend__item legend__item--demanda-sectores-primero');
    primerPunto.textContent = 'Enero';

    let segundoPunto = document.createElement('p');
    segundoPunto.setAttribute('class','legend__item legend__item--demanda-sectores-segundo');
    segundoPunto.textContent = 'Febrero';

    let tercerPunto = document.createElement('p');
    tercerPunto.setAttribute('class','legend__item legend__item--demanda-sectores-tercero');
    tercerPunto.textContent = 'Marzo';

    let cuartoPunto = document.createElement('p');
    cuartoPunto.setAttribute('class','legend__item legend__item--demanda-sectores-cuarto');
    cuartoPunto.textContent = 'Abril';

    let quintoPunto = document.createElement('p');
    quintoPunto.setAttribute('class','legend__item legend__item--demanda-sectores-quinto');
    quintoPunto.textContent = 'Mayo';

    let sextoPunto = document.createElement('p');
    sextoPunto.setAttribute('class','legend__item legend__item--demanda-sectores-sexto');
    sextoPunto.textContent = 'Junio';

    divLeyenda.appendChild(primerPunto);
    divLeyenda.appendChild(segundoPunto);
    divLeyenda.appendChild(tercerPunto);
    divLeyenda.appendChild(cuartoPunto);
    divLeyenda.appendChild(quintoPunto);
    divLeyenda.appendChild(sextoPunto);

    document.getElementById(grafico.substr(1)).appendChild(divLeyenda);
    
    //Estructura fundamental del gráfico
    let margin = {top: 5, right: 10, bottom: 20, left: 110},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom - (window.innerWidth < 425 ? 74 : 25);

    //Modificación del formato de datos
    let newData = [];

    for(let i = 0; i < data.length; i++){        
        newData.push({categoria: data[i]['sector_mostrar'], valores: [{descriptor:'junio', valor: data[i].dato_junio},
        {descriptor:'mayo', valor: data[i].dato_mayo}, 
        {descriptor:'abril', valor: data[i].dato_abril}, 
        {descriptor:'marzo', valor: data[i].dato_marzo}, {descriptor:'febrero', valor: data[i].dato_febrero}, 
        {descriptor:'enero', valor: data[i].dato_enero}]});               
    }

    let sectores = newData.map(function(d) { return d.categoria });
    let meses = newData[0].valores.map(function(d) { return d.descriptor});

    //Estructura del gráfico
    let chart = d3.select(grafico)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje Y
    let y0 = d3.scaleBand()
        .rangeRound([height,0])
        .domain(sectores)
        .paddingInner(.5)
        .align(1)

    let y1 = d3.scaleBand()
        .range([y0.bandwidth(),0])
        .domain(meses);

    let yAxis = function(g){
        g.call(d3.axisLeft(y0).tickSize(0))
        g.call(function(g){g.selectAll('.tick text')
            .style('fill', '#fff')
            .style('font-size','14.5px')
            .style('font-family','Roboto')
            .style('font-weight','300')
            .style('opacity','1')})
        g.call(function(g){g.select('.domain').attr('id', lineaEjeY).style('opacity','0')});
    }        

    chart.append("g")        
        .attr('transform', 'translate(0,0)')
        .call(yAxis);
    
    //Eje X
    let x = d3.scaleLinear()
        .range([width, 10])
        .domain([20,-100]);

    let xAxis = function(g){
        g.call(d3.axisBottom(x).ticks(4))
        g.call(function(g){g.select('.domain').remove()})
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 0.5)
            .attr("stroke", "#343434")
            .attr("stroke-width", "1")
            .attr("y1", '0%')
            .attr("y2", '-' + (document.getElementById(lineaEjeY).getBoundingClientRect().height) + '')});
    }        

    chart.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);
    
    //Visualización de datos
    let slice = chart.selectAll(".slice")
      .data(newData)
      .enter().append("g")
      .attr("class", "g")
      .attr("transform",function(d) { return "translate(0," + y0(d.categoria) + ")"; });

    slice.selectAll("rect")
        .data(function(d) { return d.valores; })
        .enter()
        .append("rect")
        .attr("width", function(d) { return Math.abs(x(d.valor) - x(0)); })
        .attr('height', y1.bandwidth())
        .style('fill',function(d) {return d.descriptor == 'enero' ? '#113F26' : d.descriptor == 'febrero' ? '#1C6940' : d.descriptor == 'marzo' ? '#2EAB69' : d.descriptor == 'abril' ? '#59FBA5' : d.descriptor == 'mayo' ? '#B8FFD9' : '#fff'})
        .attr('x', function(d) { return d.valor > 0 ? x(0) : x(d.valor)})
        .attr('y', function(d) { return y1(d.descriptor)})
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipDemanda.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipDemanda.html('<span style="font-weight: 900;">' + d.valor.toFixed(2).replace('.',',') + '</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipDemanda.style('display','none').style('opacity','0');
        })
        
}

function getDemandaCCAA(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart5-3' : '#chart5-3';
    let menosAltura = 0;
    
    //Desarrollo de botones para interior
    if(nivelNavegacion != 'area'){
        menosAltura = 57.5;

        // Botones en Interior
        let grafico2 = document.getElementById('chart5-3');
        let grupoBotones = document.createElement('div');
        grupoBotones.classList.add('chart-buttons');

        let firstButton = document.createElement('button');
        firstButton.textContent = 'Variación anual';
        firstButton.classList.add('button');
        firstButton.classList.add('button__demanda');
        firstButton.classList.add('button-active');
        let secondButton = document.createElement('button');
        secondButton.textContent = 'Variación mensual';
        secondButton.classList.add('button');
        secondButton.classList.add('button__demanda');

        grupoBotones.appendChild(firstButton);
        grupoBotones.appendChild(secondButton);
        grafico2.insertBefore(grupoBotones, grafico2.firstChild);

        //Asociamos los botones a un evento que cambiará la forma de datos
        firstButton.addEventListener('click', function() {
            updateMap('variacion-anual');
            firstButton.classList.add('button-active');
            secondButton.classList.remove('button-active');
            getOutTooltips();
        });
        secondButton.addEventListener('click', function() {
            updateMap('variacion-mensual');
            secondButton.classList.add('button-active');
            firstButton.classList.remove('button-active');
            getOutTooltips();
        });
    }

    //Creación de leyenda        
    let divLeyenda = document.createElement('div');
    divLeyenda.setAttribute('class','legend');

    let primerPunto = document.createElement('p');
    primerPunto.setAttribute('class','legend__item legend__item--demanda-primero');
    primerPunto.textContent = 'Menos de -10';

    let segundoPunto = document.createElement('p');
    segundoPunto.setAttribute('class','legend__item legend__item--demanda-segundo');
    segundoPunto.textContent = '-10 a -5';

    let tercerPunto = document.createElement('p');
    tercerPunto.setAttribute('class','legend__item legend__item--demanda-tercero');
    tercerPunto.textContent = '-5 a 0';

    let cuartoPunto = document.createElement('p');
    cuartoPunto.setAttribute('class','legend__item legend__item--demanda-cuarto');
    cuartoPunto.textContent = 'Más de 0';

    divLeyenda.appendChild(primerPunto);
    divLeyenda.appendChild(segundoPunto);
    divLeyenda.appendChild(tercerPunto);
    divLeyenda.appendChild(cuartoPunto);

    document.getElementById(grafico.substr(1)).appendChild(divLeyenda);

    //Estructura fundamental del gráfico
    let margin = {top: 20, right: 20, bottom: 20, left: 30},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.parentElement.clientWidth : slideWidth),
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - (window.innerWidth < 425 ? 50 : 25) - menosAltura;
    
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width)
        .attr('height', height);

    function initMap(){
        d3.json(mapaAutonomias, function (error, mapa) {
            if (error) throw error;
            //No hay datos para Ceuta y Melilla, por lo que no es necesario recuperarlas
            let coordenadasMapa = topojson.feature(mapa, mapa.objects.autonomous_regions);
            let datosMapa = coordenadasMapa.features;
    
            data.forEach(function(item) {
                datosMapa.forEach(function(item2){ //Sólo quedan sin datos Ceuta y Melilla
                    if(parseInt(item.id_ccaa) == parseInt(item2.id)){
                        item2.ccaa = item.ccaa;
                        item2.variacion_anual = item.variacion_anual;
                        item2.variacion_mensual = item.variacion_mensual;
                    }
                })
            });
            
            const projection = d3.geoConicConformalSpain().fitSize([width,height], coordenadasMapa);
            const path = d3.geoPath(projection);
    
            chart.selectAll(".region")
                .data(datosMapa)
                .enter()
                .append("path")
                .attr('id', function(d) {return d.ccaa})
                .attr("d", path)
                .style("fill", function(d) {return d.variacion_anual < -10 ? '#144229' : d.variacion_anual < -5 && d.variacion_anual >= -10 ? '#2C7B51' : d.variacion_anual <= 0 && d.variacion_anual >= -5 ? '#59FBA5' : '#B8FFD9'})
                .style("stroke", "#000")
                .style("stroke-width", ".6px")
                .on('touchstart mouseover', function(d) { 
                    let bodyWidth = parseInt(d3.select('body').style('width'));
                    let mapHeight = parseInt(d3.select('body').style('height'));
            
                    // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
                    // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
            
                    tooltipDemanda.transition()     
                        .duration(200)
                        .style('display', 'block')
                        .style('opacity','0.9')
                    tooltipDemanda.html('<span style="font-weight: 300;">' + d.ccaa + '</span><br><span style="font-weight: 900;">' + d.variacion_anual.toFixed(2).replace('.',',') + '%</span>')
                        .style("left", ""+ (d3.event.pageX - 25) +"px")     
                        .style("top", ""+ (d3.event.pageY - 35) +"px");
                })
                .on('touchend mouseout mouseleave', function(d) {
                    tooltipDemanda.style('display','none').style('opacity','0');
                })
        })
        // d3.json(mapaAutonomias).then(function(mapa) {
            
        // }).catch(function(error) {console.log(error)});
    }

    function updateMap(buttonType){
        
        chart.selectAll("path")
            .style("fill", function(d) {
                if(buttonType == 'variacion-anual'){
                    return d.variacion_anual < -10 ? '#144229' : d.variacion_anual < -5 && d.variacion_anual >= -10 ? '#2C7B51' : d.variacion_anual <= 0 && d.variacion_anual >= -5 ? '#59FBA5' : '#B8FFD9';
                } else {
                    return d.variacion_mensual < -10 ? '#144229' : d.variacion_mensual < -5 && d.variacion_mensual >= -10 ? '#2C7B51' : d.variacion_mensual <= 0 && d.variacion_mensual >= -5 ? '#59FBA5' : '#B8FFD9';
                }
            })
            .on('touchstart mouseover', function(d) {
                //Dato a mostrar
                let dato = '';
                if(buttonType == 'variacion-anual'){
                    dato = d.variacion_anual;
                } else {
                    dato = d.variacion_mensual;
                }

                //Disposición del tooltip
                let bodyWidth = parseInt(d3.select('body').style('width'));
                let mapHeight = parseInt(d3.select('body').style('height'));
        
                // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
                // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
        
                tooltipDemanda.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                    tooltipDemanda.html('<span style="font-weight: 900;">' + dato.toFixed(2).replace('.',',') + '%</span>')
                    .style("left", ""+ (d3.event.pageX - 25) +"px")     
                    .style("top", ""+ (d3.event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDemanda.style('display','none').style('opacity','0');
            })
    }

    initMap();    
}

function getDemandaEurozona(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart5-4' : '#chart5-4';
    let lineaEjeX = nivelNavegacion == 'area' ? 'area-demanda-eurozona-ejeX' : 'slider-demanda-eurozona-ejeX';
    
    //Estructura fundamental del gráfico
    let margin = {top: 5, right: 10, bottom: 82.5, left: 25},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;
    
    function comparativa(a,b){
        const datoA = a.consumo;
        const datoB = b.consumo;

        let comparacion = 0;
        if (datoA < datoB) {
            comparacion = 1;
        } else if (datoA >= datoB) {
            comparacion = -1;
        }
        return comparacion;
    }

    let newData = data.sort(comparativa);   

    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleBand()
        .range([0,width])
        .domain(d3.range(data.length));

    let x2 = d3.scaleBand()
        .range([0,width])
        .domain(data.map(function(d) { return d.pais }))

    let xAxis = function(g){
        g.call(d3.axisBottom(x2))
        g.call(function(g){g.selectAll('.tick text').style('opacity','initial').attr('text-anchor','end').attr('transform', 'rotate(-90)').attr('y', - ((x2.bandwidth() / 2) - (nivelNavegacion == 'area' ? 1.5 : 4.5)))})
        g.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {return d == 'España' ? '1' : '0.5'})})
        g.call(function(g){g.selectAll('.tick line').remove()})
        g.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')});
    }        

    chart.append('g')
        .attr('transform', 'translate(0, ' + (height+10) + ')')
        .call(xAxis);
    
    //Eje Y
    let y = d3.scaleLinear()
        .domain([d3.min(data, function(d) {return d.consumo}),d3.max(data, function(d) {return d.consumo})])
        .range([height,0])
        .nice();

    let yAxis = function(g){
        g.call(d3.axisLeft(y).ticks(6))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.select('.domain').remove()})
        g.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 0.5)
            .attr("stroke", function(d) {return d == 0 ? '#d9d9d9' : '#343434'})
            .attr("x1", '0%')
            .attr("x2", '' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')});
    }        

    chart.append('g')
        .call(yAxis);

    chart.selectAll('.barras')
        .data(data)
        .enter()
        .append('rect')
        .attr('fill', "#59fba5")
        .attr('y', function(d) { return y(Math.max(0,d.consumo)) })
        .attr('x', function(d,i) { return x(i)})
        .attr('height', function(d) {return Math.abs(y(d.consumo) - y(0));})
        .attr('width', x.bandwidth() / 2)
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipDemanda.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipDemanda.html('<span style="font-weight: 900;">' + d.consumo.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipDemanda.style('display','none').style('opacity','0');
        })
}

//Sectores
function getSectoresCambioAfiliacion(data, nivelNavegacion){ //Gráfico lollipop separado a la mitad
    let grafico = nivelNavegacion == 'area' ? '#area-chart6-1' : '#chart6-1';
    
    //Estructura fundamental del gráfico
    let margin = {top: 0, right: 15, bottom: 15, left: 115};
    let width = 0;
    let height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight - 57.5) - margin.top - margin.bottom;
    
    let x, xAxis;
    let tipo = 'porcentaje';

    if(window.innerWidth <= 835 || nivelNavegacion != 'area'){
        if(nivelNavegacion != 'area'){
            //Creación de dos botones para Interior
            let grafico2 = document.getElementById('chart6-1');
            let grupoBotones = document.createElement('div');
            grupoBotones.classList.add('chart-buttons');

            let firstButton = document.createElement('button');
            firstButton.textContent = 'Porcentaje';
            firstButton.classList.add('button');
            firstButton.classList.add('button__sectores');
            firstButton.classList.add('button-active');
            let secondButton = document.createElement('button');
            secondButton.textContent = 'Absoluto';
            secondButton.classList.add('button');
            secondButton.classList.add('button__sectores');

            grupoBotones.appendChild(firstButton);
            grupoBotones.appendChild(secondButton);
            grafico2.insertBefore(grupoBotones, grafico2.firstChild);

            //Asociamos los botones a un evento que cambiará la forma de datos
            firstButton.addEventListener('click', function() {
                updateChart('porcentaje');
                firstButton.classList.add('button-active');
                secondButton.classList.remove('button-active');
                getOutTooltips();
            });
            secondButton.addEventListener('click', function() {
                updateChart('absoluto');
                secondButton.classList.add('button-active');
                firstButton.classList.remove('button-active');
                getOutTooltips();
            });
        } else {
            document.getElementById('area-chart6-1').style.display = 'flex';
            document.getElementById('area-chart6-1').style.justifyContent = 'center';
        }

        //Definiciones para luego jugar con el update
        width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right;
        
        x = d3.scaleLinear()
            .domain([-17.5, 10])
            .range([0, width]);
        xAxis = function(g){
            g.call(d3.axisBottom(x).ticks(4))
            g.call(function(g){g.selectAll('.domain').remove()})
            g.call(function(g){g.selectAll('.tick line').remove()})
            g.attr("transform", "translate(0," + (height - 5) + ")");
        }
        
        unicoGrafico(data, width);
    } else {
        //Estilos ad hoc
        document.getElementById(grafico.substr(1)).style.display = 'flex';
        document.getElementById(grafico.substr(1)).style.justifyContent = 'space-around';
        width = (
            (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right) / 
            ( window.innerWidth >= 1200 ? 2.5 : window.innerWidth >=768 && window.innerWidth < 939 ? 4.25 : window.innerWidth >= 939 && window.innerWidth < 1200 ? 3 : 1);
        
        let primerGrupo = data.slice(0, data.length / 2);
        let segundoGrupo = data.slice(data.length / 2);
        let newData = [primerGrupo,segundoGrupo];        

        dobleGrafico(newData, width);
    }
    
    //Funciones de visualización de datos    
    function unicoGrafico(data, width){
        // append the svg object to the body of the page
        let svg = d3.select(grafico)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //X axis
        svg.append("g")
            .attr('class', 'x-axis')
            .call(xAxis);
        // Y axis
        let y = d3.scaleBand()
        .range([0, height])
        .domain(data.map(function(d) { return d.sector_mostrar; }))
        .paddingOuter(0);

        let yAxis = function(g){
            g.call(d3.axisLeft(y))
            g.call(function(g){g.selectAll('.domain').remove()})
            g.call(function(g){g.selectAll('.tick text').style('font-size', (nivelNavegacion == 'area' ? '13.5px' : '14px')).style('opacity','1').style('font-weight','300')})
            g.call(function(g){g.selectAll('.tick line')
                .attr('x2','0')
                .attr('x1','100%')
                .style('stroke','#343434')
                .style('stroke-dasharray','2')});
        }            

        svg.append("g")
            .call(yAxis);

        // Line >> With gradient
        svg.selectAll("myline")
        .data(data)
        .enter()
        .append("line")
            .attr("class", "my-lines")
            .attr("x1", function(d) { return x(0); })
            .attr("x2", function(d) { return x(d.data_porcentaje); })
            .attr("y1", function(d) { return y(d.sector_mostrar) + (y.bandwidth() / 2); })
            .attr("y2", function(d) { return y(d.sector_mostrar) + (y.bandwidth() / 2); })
            .attr("stroke", "#d8d8d8")
            .attr("stroke-width", "1px")

        // Circles of variable 1
        svg.selectAll("mycircle")
            .data(data)
            .enter()
            .append("circle")
                .attr("class", "my-circles-1")
                .attr("cx", function(d) { return x(0); })
                .attr("cy", function(d) { return y(d.sector_mostrar) + (y.bandwidth() / 2); })
                .attr("r", "3")
                .style("fill", "#e4fbff")

        //Circles of 2
        svg.selectAll("mycircle")
            .data(data)
            .enter()
            .append("circle")
                .attr("class", "my-circles-2")
                .attr("cx", function(d) { return x(d.data_porcentaje); })
                .attr("cy", function(d) { return y(d.sector_mostrar) + (y.bandwidth() / 2); })
                .attr("r", "3")
                .style("fill", "#88ebff")
                .on('touchstart mouseover', function(d) {
                    let dato = '';
                    if(tipo == 'porcentaje'){
                        dato = getNumberWithCommas(d.data_porcentaje.toFixed(2).replace('.',',')) + "%";
                    } else {
                        dato = getNumberWithCommas(d.data_absoluto.toFixed(2).replace('.',','));
                    }

                    let bodyWidth = parseInt(d3.select('body').style('width'));
                    let mapHeight = parseInt(d3.select('body').style('height'));
            
                    // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
                    // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
            
                    tooltipSectores.transition()     
                        .duration(200)
                        .style('display', 'block')
                        .style('opacity','0.9')
                    tooltipSectores.html('<span style="font-weight: 900;">' + dato + '</span>')
                        .style("left", ""+ (d3.event.pageX - 25) +"px")     
                        .style("top", ""+ (d3.event.pageY - 35) +"px");
                })
                .on('touchend mouseout mouseleave', function(d) {
                    tooltipSectores.style('display','none').style('opacity','0');
                })

    }

    function dobleGrafico(newData, width){
        newData.map(function(data2) {
            // append the svg object to the body of the page
            let svg = d3.select(grafico)
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
            //X axis
            let x = d3.scaleLinear()
            .domain([-17.5, 10])
            .range([0, width]);
    
            let xAxis = function(g){
                g.call(d3.axisBottom(x).ticks(5))
                g.call(function(g){g.selectAll('.domain').remove()})
                g.call(function(g){g.selectAll('.tick line').remove()})
                g.attr("transform", "translate(0," + (height - 5) + ")");
            }                
    
            svg.append("g")
                .call(xAxis);
    
            // Y axis
            let y = d3.scaleBand()
            .range([0, height])
            .domain(data2.map(function(d) { return d.sector_mostrar; }))
            .paddingOuter(0);
    
            let yAxis = function(g){
                g.call(d3.axisLeft(y))
                g.call(function(g){g.selectAll('.domain').remove()})
                g.call(function(g){g.selectAll('.tick text').style('font-size','15px').style('opacity','1').style('font-weight','300')})
                g.call(function(g){g.selectAll('.tick line')
                    .attr('x2','0')
                    .attr('x1','100%')
                    .style('stroke','#343434')
                    .style('stroke-dasharray','2')});
            }                
    
            svg.append("g")
                .call(yAxis);
    
            // Lines
            svg.selectAll("myline")
            .data(data2)
            .enter()
            .append("line")
                .attr("x1", function(d) { return x(0); })
                .attr("x2", function(d) { return x(d.data_porcentaje); })
                .attr("y1", function(d) { return y(d.sector_mostrar) + (y.bandwidth() / 2); })
                .attr("y2", function(d) { return y(d.sector_mostrar) + (y.bandwidth() / 2); })
                .attr("stroke", "lightgrey")
                .attr("stroke-width", "1px")
    
            // Circles of variable 1
            svg.selectAll("mycircle")
                .data(data2)
                .enter()
                .append("circle")
                    .attr("cx", function(d) { return x(0); })
                    .attr("cy", function(d) { return y(d.sector_mostrar) + (y.bandwidth() / 2); })
                    .attr("r", "3")
                    .style("fill", "#e4fbff")
    
            //Circles of 2
            svg.selectAll("mycircle")
                .data(data2)
                .enter()
                .append("circle")
                .attr("cx", function(d) { return x(d.data_porcentaje); })
                .attr("cy", function(d) { return y(d.sector_mostrar) + (y.bandwidth() / 2); })
                .attr("r", "3")
                .style("fill", "#88ebff")
                .on('touchstart mouseover', function(d) {
                    let bodyWidth = parseInt(d3.select('body').style('width'));
                    let mapHeight = parseInt(d3.select('body').style('height'));
            
                    // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
                    // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
            
                    tooltipSectores.transition()     
                        .duration(200)
                        .style('display', 'block')
                        .style('opacity','0.9')
                    tooltipSectores.html('<span style="font-weight: 900;">' + d.data_porcentaje.toFixed(2).replace('.',',') + '%</span>')
                        .style("left", ""+ (d3.event.pageX - 25) +"px")     
                        .style("top", ""+ (d3.event.pageY - 35) +"px");
                })
                .on('touchend mouseout mouseleave', function(d) {
                    tooltipSectores.style('display','none').style('opacity','0');
                })
        });
    }    

    function updateChart(type){

        if(type == 'porcentaje'){
            x.domain([-17.5,10])
        } else {
            //x.domain(d3.extent(data, function(d) { return d.data_absoluto }));
            x.domain([-260000,35000]);
        }
        tipo = type;
        
        let chart = d3.select(grafico).transition();
    
        chart.select('.x-axis')
            .duration(750)
            .call(xAxis);

        chart.selectAll('.my-lines')
            .duration(750)
            .attr("x1", function(d) { return x(0); })
            .attr("x2", function(d) { 
                if(type == 'porcentaje'){
                    return x(d.data_porcentaje);
                } else {
                    return x(d.data_absoluto);
                } 
            })

        chart.selectAll('.my-circles-1')
            .delay(750)
            .duration(1500)
            .attr("cx", x(0));

        chart.selectAll('.my-circles-2')
            .delay(750)
            .duration(1500)
            .attr("cx", function(d) {
                if(type == 'porcentaje'){
                    return x(d.data_porcentaje);
                } else {
                    return x(d.data_absoluto);
                }                
            });
    }
}

function getSectoresLlegadaViajeros(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart6-2' : '#chart6-2';
    let menosAltura = 0;
    let boton = 'total';
    //Si estamos en área no habrá botones. Si no estamos en área, sí
    if(nivelNavegacion != 'area'){
        menosAltura = 57.5;       

        // Botones en Interior
        let grafico2 = document.getElementById('chart6-2');
        let grupoBotones = document.createElement('div');
        grupoBotones.classList.add('chart-buttons');

        let firstButton = document.createElement('button');
        firstButton.textContent = 'Total turismo';
        firstButton.classList.add('button');
        firstButton.classList.add('button__sectores');
        firstButton.classList.add('button-active');
        let secondButton = document.createElement('button');
        secondButton.textContent = 'Turismo nacional';
        secondButton.classList.add('button');
        secondButton.classList.add('button__sectores');
        let thirdButton = document.createElement('button');
        thirdButton.textContent = 'Turismo extranjero';
        thirdButton.classList.add('button');
        thirdButton.classList.add('button__sectores');

        grupoBotones.appendChild(firstButton);
        grupoBotones.appendChild(secondButton);
        grupoBotones.appendChild(thirdButton);
        grafico2.insertBefore(grupoBotones, grafico2.firstChild);

        //Asociamos los botones a un evento que cambiará la forma de datos
        firstButton.addEventListener('click', function() {
            updateMap('total');
            boton = 'total';
            firstButton.classList.add('button-active');
            secondButton.classList.remove('button-active');
            thirdButton.classList.remove('button-active');
            getOutTooltips();
        });
        secondButton.addEventListener('click', function() {
            updateMap('nacional');
            boton = 'nacional';
            secondButton.classList.add('button-active');
            firstButton.classList.remove('button-active');
            thirdButton.classList.remove('button-active');
            getOutTooltips();
        });
        thirdButton.addEventListener('click', function() {
            updateMap('extranjero');
            boton = 'extranjero';
            secondButton.classList.remove('button-active');
            firstButton.classList.remove('button-active');
            thirdButton.classList.add('button-active');
            getOutTooltips();
        });
    }

    //Creación de leyenda        
    let divLeyenda = document.createElement('div');
    divLeyenda.setAttribute('class','legend');

    let primerPunto = document.createElement('p');
    primerPunto.setAttribute('class','legend__item legend__item--sectores-primero');
    primerPunto.textContent = '-100 a -90';

    let segundoPunto = document.createElement('p');
    segundoPunto.setAttribute('class','legend__item legend__item--sectores-segundo');
    segundoPunto.textContent = '-90 a -80';

    let tercerPunto = document.createElement('p');
    tercerPunto.setAttribute('class','legend__item legend__item--sectores-tercero');
    tercerPunto.textContent = '-80 a -70';

    let cuartoPunto = document.createElement('p');
    cuartoPunto.setAttribute('class','legend__item legend__item--sectores-cuarto');
    cuartoPunto.textContent = 'Más de -70';

    divLeyenda.appendChild(primerPunto);
    divLeyenda.appendChild(segundoPunto);
    divLeyenda.appendChild(tercerPunto);
    divLeyenda.appendChild(cuartoPunto);

    document.getElementById(grafico.substr(1)).appendChild(divLeyenda);

    //Estructura fundamental del gráfico
    let margin = {top: 20, right: 20, bottom: 15, left: 30},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth),
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - (window.innerWidth < 425 ? 50 : 25) - menosAltura;
        
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width)
        .attr('height', height);
    
    function initMap(){
        d3.json('https://www.ecestaticos.com/file/905fc62e1f9997a5f14d69608e3efcd2/1595321931-provincias_autonomias.json', function (error, mapa){
            if (error) throw error;
            //No hay datos para Ceuta y Melilla, por lo que no es necesario recuperarlas
            let coordenadasMapa = topojson.feature(mapa, mapa.objects.provinces);
            let datosMapa = coordenadasMapa.features;

            let coordenadasAutonomias = topojson.feature(mapa, mapa.objects.autonomous_regions);
            let datosAutonomias = coordenadasAutonomias.features;

            datosMapa.forEach(function(item) {
                data.forEach(function(item2) { //Sólo quedan sin datos Ceuta y Melilla
                    if(parseInt(item.id) == parseInt(item2.provincia_id)){
                        item.provincia = item2.provincia;
                        item.turistas_total = item2.turistas_total;
                        item.turistas_espana = item2.turistas_espana;
                        item.turistas_extranjero = item2.turistas_extranjero;
                    }
                })
            });            
    
            const projection = d3.geoConicConformalSpain().fitSize([width,height], coordenadasMapa);
            const path = d3.geoPath(projection);
    
            chart.selectAll(".region-turistas")
                .data(datosMapa)
                .enter()
                .append("path")
                .attr('class', 'region-turistas')
                .attr('id', function(d) {return d.provincia})
                .attr("d", path)
                .style("fill", function(d) {return d.provincia ? d.turistas_total < -90 ? '#1e373c' : d.turistas_total >= -90 && d.turistas_total < 80 ? '#3a6e78' : d.turistas_total >= 80 && d.turistas_total < -70 ? '#88ebff' : '#dbf9ff' : 'lightgrey'})
                .style("stroke", "#000")
                .style("stroke-width", ".6px")
                .on('touchstart mouseover', function(d) {
                    //Dato a mostrar
                    let dato = '';
                    if(boton == 'total'){
                        dato = d.turistas_total;
                    } else if (boton == 'nacional'){
                        dato = d.turistas_espana;
                    } else {
                        dato = d.turistas_extranjero;
                    }
            
                    tooltipSectores.transition()     
                        .duration(200)
                        .style('display', 'block')
                        .style('opacity','0.9')
                    tooltipSectores.html('<span style="font-weight: 300;">' + d.provincia + '</span><br><span style="font-weight: 900;">' + dato.toFixed(2).replace('.',',') + '%</span>')
                        .style("left", ""+ (d3.event.pageX - 25) +"px")     
                        .style("top", ""+ (d3.event.pageY - 35) +"px");
                })
                .on('touchend mouseout mouseleave', function(d) {
                    tooltipSectores.style('display','none').style('opacity','0');
                })
            
            chart.selectAll(".region-turistas-autonomias")
                .data(datosAutonomias)
                .enter()
                .append("path")
                .attr("d", path)
                .style("fill", 'none')
                .style("stroke", "#000")
                .style("stroke-width", ".95px")
        })
    }

    function updateMap(buttonType){
        chart.selectAll(".region-turistas")
            .style("fill", function(d) {
                if(buttonType == 'total'){
                    return d.provincia ? d.turistas_total < -90 ? '#1e373c' : d.turistas_total >= -90 && d.turistas_total < 80 ? '#3a6e78' : d.turistas_total >= 80 && d.turistas_total < -70 ? '#88ebff' : '#dbf9ff' : 'lightgrey';
                } else if (buttonType == 'nacional'){
                    return d.provincia ? d.turistas_espana < -90 ? '#1e373c' : d.turistas_espana >= -90 && d.turistas_espana < 80 ? '#3a6e78' : d.turistas_espana >= 80 && d.turistas_espana < -70 ? '#88ebff' : '#dbf9ff' : 'lightgrey';
                } else {
                    return d.provincia ? d.turistas_extranjero < -90 ? '#1e373c' : d.turistas_extranjero >= -90 && d.turistas_extranjero < 80 ? '#3a6e78' : d.turistas_extranjero >= 80 && d.turistas_extranjero < -70 ? '#88ebff' : '#dbf9ff' : 'lightgrey';
                }
            })
    }

    //Por defecto // El updateMap se asocia a los botones (a los que habrá asociar clases)
    initMap();    
}

function getSectoresValorAnadido(data, nivelNavegacion){
    let grafico = nivelNavegacion == 'area' ? '#area-chart6-3' : '#chart6-3';
    let lineaEjeY = nivelNavegacion == 'area' ? 'area-sectores-va-ejeY' : 'slider-sectores-va-ejeY';

    //Estructura fundamental del gráfico
    let margin = {top: 15, right: 10, bottom: 15, left: 110},
    width = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth) - margin.left - margin.right,
    height = (nivelNavegacion == 'area' ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight) - margin.top - margin.bottom;

    // append the svg object to the body of the page
    let chart = d3.select(grafico)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // Y axis
    let y = d3.scaleBand()
    .range([0, height])
    .domain(data.map(function(d) { return d.sector_mostrar; }));

    let yAxis = function(g){
        g.call(d3.axisLeft(y))
        g.call(function(g){g.selectAll('.domain').attr('id', lineaEjeY).style('opacity','0')})
        g.call(function(g){g.selectAll('.tick text').style('font-size','15px').style('opacity','1').style('font-weight','300')})
        g.call(function(g){g.selectAll('.tick line').remove()});
    }        

    chart.append("g")
    .call(yAxis);

    //X axis
    let x = d3.scaleLinear()
    .domain([-50, 5])
    .range([0, width]);
    
    let xAxis = function(g){
        g.call(d3.axisBottom(x).ticks(5))
        g.call(function(g){g.selectAll('.domain').remove()})
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d){return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 0.5)
            .attr("stroke", "#343434")
            .attr("stroke-width", "1")
            .attr("y1", '0%')
            .attr("y2", '-' + (document.getElementById(lineaEjeY).getBoundingClientRect().height) + '')})
        g.attr("transform", "translate(0," + (height - 5) + ")");
    }
    
    chart.append("g")
        .call(xAxis);

    // Lines
    chart.selectAll("myline")
    .data(data)
    .enter()
    .append("line")
        .attr("x1", function(d) { return x(0); })
        .attr("x2", function(d) { return x(d.variacion); })
        .attr("y1", function(d) { return y(d.sector_mostrar) + (y.bandwidth() / 2); })
        .attr("y2", function(d) { return y(d.sector_mostrar) + (y.bandwidth() / 2); })
        .attr("stroke", "#88ebff")
        .attr("stroke-width", "4px")
        .on('touchstart mouseover', function(d){
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipSectores.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipSectores.html('<span style="font-weight: 900;">' + d.variacion.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipSectores.style('display','none').style('opacity','0');
        })        
}