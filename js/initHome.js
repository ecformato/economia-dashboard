//Constantes y variables relativas a la Home
const dataHome = 'https://api.elconfidencial.com/service/resource/dashboard-economia-portada/';
const commonElements = 'https://api.elconfidencial.com/service/resource/dashboard-economia-elementos-comunes/';
const dataProduccion = 'https://api.elconfidencial.com/service/resource/dashboard-economia-produccion/';
const dataExpectativas = 'https://api.elconfidencial.com/service/resource/dashboard-economia-expectativas/';
const dataCuentasPublicas = 'https://api.elconfidencial.com/service/resource/dashboard-economia-cuentas-publicas/';

const dataHomeLocal = './local-data/portada.tsv';
const commonElementsLocal = './local-data/elementos-comunes.tsv';
const dataProduccionLocal = './local-data/produccion.tsv';
const dataCuentasPublicasLocal = './local-data/cuentas-publicas.tsv';
const dataExpectativasLocal = './local-data/expectativas.tsv';

/* Helpers */
let meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
let mesesReducido = ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'];

let pibTrimestralProduccion = [], pibEurozonaExpectativas = [], deficitEstadoCuentasPublicas = [], numeroAfiliadosEmpleo = [], evolucionConsumoDemanda = [], variacionAfiliacionSectores = [];
let produccionPib = [], produccionPibEurozona = [], produccionInversionProductiva = [], produccionEmpresasActivas = [], produccionProdIndustrial = [], produccionPibEmpleo = [];
let expectativasPrevisiones = [], expectativasPMI = [], expectativasConfianzaConsumidor = [], expectativasHogaresEconomia = [], expectativasHogaresParo = [];
let cuentasDeficitHistorico = [], cuentasRecaudacion = [], cuentasDeudaPublica = [], cuentasDeficitCCAA = [];
let commonElementsMap = new Map();

let currentArea = 'home';
let env = 'pro';
//let env = 'local';
if(env == 'local'){
    getData(commonElementsLocal, 'commonElements');
    getData(dataProduccionLocal, 'dataProduccion');
    getData(dataExpectativasLocal, 'dataExpectativas');
    getData(dataCuentasPublicasLocal, 'dataCuentasPublicas');
    getData(dataHomeLocal, 'dataHome');
} else {
    getData(commonElements, 'commonElements');
    getData(dataProduccion, 'dataProduccion');
    getData(dataExpectativas, 'dataExpectativas');
    getData(dataCuentasPublicas, 'dataCuentasPublicas');
    getData(dataHome, 'dataHome');
}

/////
const produccion = ['produccionPib', 'produccionPibEurozona', 'produccionInversionProductiva', 'produccionEmpresasActivas', 'produccionProdIndustrial', 'produccionPibEmpleo']; 
const expectativas = ['expectativasPrevisiones', 'expectativasPMI', 'expectativasConfianzaConsumidor', 'expectativasHogaresEconomia', 'expectativasHogaresParo'];
const cuentas_publicas = ['cuentasDeficitHistorico', 'cuentasRecaudacion', 'cuentasDeudaPublica', 'cuentasDeficitCCAA'];

//////
////// Configuración Modal
//////
let overlay = document.getElementsByClassName('overlay')[0];
let modal = document.getElementsByClassName('modal')[0];
showModal();

let modalClose = document.getElementsByClassName('modal__close')[0];
modalClose.addEventListener('click', function(e) {
    e.preventDefault();
    hideModal();     
});

function showModal() {
    overlay.classList.add('overlay--visible');
    modal.classList.add('modal--visible');
}
function hideModal() {
    overlay.classList.remove('overlay--visible');
    modal.classList.remove('modal--visible');
}

//////
////// Configuración RRSS
//////
let rrss = document.getElementsByClassName('rrss')[0];
rrss.style.width = document.getElementsByClassName('aside')[0].offsetWidth+"px";
setRRSSLinks()

function setRRSSLinks() {
    var urlPage = window.location.href;

    //Facebook
    var shareFB = document.getElementById("shareFB")
    var fbHref = "https://www.facebook.com/sharer/sharer.php?u="+urlPage
    shareFB.setAttribute("href",fbHref)

    //Twitter
    var shareTW = document.getElementById("shareTW")
    var twText = shareTW.getAttribute("data-text")
    var twHref = "https://twitter.com/intent/tweet?url="+urlPage+"&text="+twText+"&original_referer="+urlPage
    shareTW.setAttribute("href",twHref)

    //Linkedin
    var shareLK = document.getElementById("shareLK")
    var lkText = shareLK.getAttribute("data-text")
    var lkHref = "https://www.linkedin.com/shareArticle?mini=true&url="+urlPage+"&title="+lkText+"&summary=&source="
    shareLK.setAttribute("href",lkHref)

    //WhatsApp
    var shareWA = document.getElementById("shareWA")
    var waText = shareWA.getAttribute("data-text")
    var waHref = "https://api.whatsapp.com/send?text="+waText+" "+urlPage
    shareWA.setAttribute("href",waHref)
}

//////
////// Configuración Menú móvil
//////
let aside = document.getElementsByClassName('aside')[0];

let menuIcon = document.getElementsByClassName('menu__icon')[0];
menuIcon.addEventListener('click', function(e) {
    e.preventDefault();
    showMenu();
    getOutTooltips();
});

function showMenu() {
    aside.classList.add('aside--visible');
    rrss.classList.add('rrss--visible');
}

function hideMenu() {
    aside.classList.remove('aside--visible');
    rrss.classList.remove('rrss--visible');
}

//Detectamos que hacemos clic fuera del menú una vez que esté está desplegado
if ( isMobile() ) {
    document.addEventListener("click", function(evt) {
        if ( aside.classList.contains("aside--visible") ) {
            var targetElement = evt.target;  // clicked element

            do {
                if ( (targetElement == aside) || (targetElement == menuIcon) ) {
                    // This is a click inside. Do nothing, just return.
                    //console.log("Clic en aside o menuIcon!");
                    return;
                }
                // Go up the DOM
                targetElement = targetElement.parentNode;
            } while (targetElement);

            // This is a click outside.
            //console.log("Clic fuera de aside!");
            hideMenu();
        }
    });
}

/*!
 * swiped-events.js - v1.1.0
 * Pure JavaScript swipe events
 * https://github.com/john-doherty/swiped-events
 * @inspiration https://stackoverflow.com/questions/16348031/disable-scrolling-when-touch-moving-certain-element
 * @author John Doherty <www.johndoherty.info>
 * @license MIT
 */
!function(t,e){"use strict";"function"!=typeof t.CustomEvent&&(t.CustomEvent=function(t,n){n=n||{bubbles:!1,cancelable:!1,detail:void 0};var u=e.createEvent("CustomEvent");return u.initCustomEvent(t,n.bubbles,n.cancelable,n.detail),u},t.CustomEvent.prototype=t.Event.prototype),e.addEventListener("touchstart",function(t){if("true"===t.target.getAttribute("data-swipe-ignore"))return;o=t.target,l=Date.now(),n=t.touches[0].clientX,u=t.touches[0].clientY,a=0,i=0},!1),e.addEventListener("touchmove",function(t){if(!n||!u)return;var e=t.touches[0].clientX,l=t.touches[0].clientY;a=n-e,i=u-l},!1),e.addEventListener("touchend",function(t){if(o!==t.target)return;var e=parseInt(o.getAttribute("data-swipe-threshold")||"20",10),s=parseInt(o.getAttribute("data-swipe-timeout")||"500",10),r=Date.now()-l,c="";Math.abs(a)>Math.abs(i)?Math.abs(a)>e&&r<s&&(c=a>0?"swiped-left":"swiped-right"):Math.abs(i)>e&&r<s&&(c=i>0?"swiped-up":"swiped-down");""!==c&&o.dispatchEvent(new CustomEvent(c,{bubbles:!0,cancelable:!0}));n=null,u=null,l=null},!1);var n=null,u=null,a=null,i=null,l=null,o=null}(window,document);

//Detectamos que hacemos swipe en el menú para cerrar una vez que esté está desplegado
if ( isMobile() ) {
    aside.addEventListener('swiped-left', function(evt) {
        if ( aside.classList.contains("aside--visible") ) {
            hideMenu();
        }
    });
}

//////
////// Implementación de lógica para menú y enlaces 'Ver más' de portada
//////
let menuLinks = document.getElementsByClassName('menu__link');
let cardLinks = document.getElementsByClassName('card__link');
for(let i = 0; i < menuLinks.length; i++){
    menuLinks[i].addEventListener('click', function(e) {
        e.preventDefault();
        showSpecificArea(e.target);  
        if ( isMobile() ) {
            hideMenu();    
        }
        getOutTooltips();
    });
}
for(let i = 0; i < cardLinks.length; i++){
    cardLinks[i].addEventListener('click', function(e) {
        e.preventDefault();
        showSpecificArea(e.target);
        getOutTooltips();
    });
}

let submenuLinks = document.getElementsByClassName('submenu__link');
for(let i = 0; i < submenuLinks.length; i++){
    submenuLinks[i].addEventListener('click', function(e) {
        e.preventDefault();
        drawSlider(e.target);
        getOutTooltips();
    });
}

function showSubmenu(menuLink) {
    let currentSubmenuActive = document.getElementsByClassName('submenu--active')
    if ( currentSubmenuActive.length > 0 ) {
        if ( !isResponsive() ) {
            currentSubmenuActive[0].style.maxHeight = 0;
        }
        currentSubmenuActive[0].classList.remove("submenu--active");
    } 
    let submenu = menuLink.nextElementSibling;
    if (submenu !== null) {
        if ( !isResponsive() ) {
            let submenuHeight = (submenu.childElementCount * 28) + ( (submenu.childElementCount -1) * 2 );
            submenu.style.maxHeight = submenuHeight+"px";
        }
        submenu.classList.add("submenu--active");
    }
}

function updateCurrentMenuLink(menuLink) {
    //Desactivamos submenú activo en caso de que exista
    let currentSubmenuLinkActive = document.getElementsByClassName('submenu__link--active')
    if ( currentSubmenuLinkActive.length > 0 ) {
        currentSubmenuLinkActive[0].classList.remove("submenu__link--active");
    }
    //Actualizamos el menú activo por el nuevo seleccionado
    let currentMenuLink = document.getElementsByClassName("menu__link--active")[0];
    currentMenuLink.classList.remove("menu__link--active");
    menuLink.classList.add("menu__link--active");
}

//////
////// Dibujo de la Home
//////

function createHome() {
    //Pintura de gráficos cuando ya tengan sus correspondientes bloques
    getPibProduccionHome(pibTrimestralProduccion, produccionPib);
    getDeficitCuentasPublicasHome(deficitEstadoCuentasPublicas, cuentasDeficitHistorico);
    getAfiliadosEmpleoHome(numeroAfiliadosEmpleo);
    getPibExpectativasHome(pibEurozonaExpectativas, expectativasPrevisiones);
    getConsumoDemandaHome(evolucionConsumoDemanda);
    getAfiliacionSectoresHome(variacionAfiliacionSectores);

    let graficos = document.getElementsByClassName('card__content');

    for(let i = 0; i < graficos.length; i++){
        graficos[i].addEventListener('mouseleave', function() {getOutTooltips()})
    }
}

/* Dibujo de los datos/gráficos */
function getPibProduccionHome(data2, data){
        if(window.innerWidth <= 767){
        let grafico = '#homeProduccion__data-mobile';
        let lineaEjeX = 'home-crecimiento-trim-ejeX';
        
        //Estructura fundamental del gráfico
        let margin = {top: 20, right: 15, bottom: 20, left: 20},
        width = document.getElementById(grafico.substr(1)).parentElement.clientWidth - margin.left - margin.right,
        height = document.getElementById(grafico.substr(1)).parentElement.clientHeight - margin.top - margin.bottom;

        //Inicialización del gráfico
        let chart = d3.select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top +')');

        //Eje X
        let x = d3.scaleBand()
            .range([0,width])
            .domain(d3.range(data.length));

        let x2 = d3.scaleTime()
            .range([0,width])
            .domain(d3.extent(data, function(d) { return formatTime(d.trimestre_v2) }));

        let xAxis = function(svg){
            svg.call(d3.axisBottom(x2).ticks(8).tickFormat(function(d){ return d.getFullYear(); }))
            svg.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {if(d.getFullYear() == 1996 || d.getFullYear() == 2004 || d.getFullYear() == 2012 ||d.getFullYear() == 2020){return 1} else {return 0}})})
            svg.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d) {return d.getFullYear() == 2020 ? 'end' : 'middle' })})
            svg.call(function(g){g.selectAll('line').remove()})
            svg.call(function(g){g.select('.domain').style('opacity','0').attr('id', lineaEjeX)});
        }        

        chart.append('g')            
            .attr('transform', 'translate(0, ' + height + ')')
            .call(xAxis);
        
        //Eje Y
        let y0 = Math.max(Math.abs(d3.min(data, function(d) {return d.pib_data})), Math.abs(d3.max(data, function(d) {return d.pib_data})));

        let y = d3.scaleLinear()
            .domain([-y0,2])
            .range([height,0])
            .nice();

        let yAxis = function(svg){
            svg.call(d3.axisLeft(y).ticks(6))
            svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
            svg.call(function(g){g.select('.domain').remove()})
            svg.call(function(g){g.selectAll('.tick line')
                .attr("stroke-opacity", 0.5)
                .attr("stroke", function(d) {return d == 0 ? '#d9d9d9' : '#343434'})
                .attr("x1", '0%')
                .attr("x2", '' + (document.getElementById('' + lineaEjeX + '').getBoundingClientRect().width) + '')});
        }        

        chart.append('g')
            .attr("transform", 'translate(0,0)')
            .call(yAxis);

        chart.selectAll('.barras')
            .data(data)
            .enter()
            .append('rect')
            .attr('fill', function(d) {return d.pib_data < 0 ? '#fde5e1' : '#ffbec3'})
            .attr('y', function(d) { return y(Math.max(0,d.pib_data)) })
            .attr('x', function(d,i) { return x(i)})
            .attr('height', function(d) {return Math.abs(y(d.pib_data) - y(0));})
            .attr('width', x.bandwidth() / 2)
            .on('touchstart mouseover', function(d) {
                let bodyWidth = parseInt(d3.select('body').style('width'));
                let mapHeight = parseInt(d3.select('body').style('height'));
        
                // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
                // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
        
                tooltipProduccion.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipProduccion.html('<span style="font-weight: 300;">' + d.trimestre + '</span><br><span style="font-weight: 900;">' + d.pib_data.toFixed(2).replace('.',',') + '</span>')
                    .style("left", ""+ (d3.event.pageX - 25) +"px")     
                    .style("top", ""+ (d3.event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipProduccion.style('display','none').style('opacity','0');
            })
    } else {
        //Trabajar con homeProduccion__data y homeProduccion__arrow
        document.getElementById('homeProduccion__data').textContent = data2[1].pib_data.toFixed(1) + "%";
        //¿Es mejor o peor que el dato anterior?
        let prueba = data2[1].pib_data < 0;
        prueba ? document.getElementById('homeProduccion__arrow').innerHTML = '<img src="https://www.ecestaticos.com/file/754f36fc84032ae4e6444aa877433e19/1591887985-arrow-down.svg" width="32" height="12" alt="Flecha en descenso" title="Descenso respecto al dato anterior">' : document.getElementById('homeProduccion__arrow').innerHTML = '<img src="https://www.ecestaticos.com/file/7e6a8ec1af77d3616135a0022edb227a/1591888407-arrow-up.svg" width="32" height="12" alt="Flecha en ascenso" title="Ascenso respecto al dato anterior">'; 
    }
}

function getPibExpectativasHome(data2, data){
    if(window.innerWidth <= 767){
        let grafico = '#homeExpectativas__data-mobile';
        let lineaEjeY = 'home-expectativas-previsiones-ejeY';

        //Creación de leyenda        
        let divLeyenda = document.createElement('div');
        divLeyenda.setAttribute('class','legend-three');

        let primerPunto = document.createElement('p');
        primerPunto.setAttribute('class','legend__item legend__item--expectativas-primero');
        primerPunto.textContent = 'III trim.';

        let segundoPunto = document.createElement('p');
        segundoPunto.setAttribute('class','legend__item legend__item--expectativas-segundo');
        segundoPunto.textContent = 'IV trim.';

        let tercerPunto = document.createElement('p');
        tercerPunto.setAttribute('class','legend__item legend__item--expectativas-tercero');
        tercerPunto.textContent = 'I trim. 2021';

        divLeyenda.appendChild(primerPunto);
        divLeyenda.appendChild(segundoPunto);
        divLeyenda.appendChild(tercerPunto);

        document.getElementById(grafico.substr(1)).appendChild(divLeyenda);
        
        //Estructura fundamental del gráfico
        let margin = {top: 10, right: 90, bottom: 27.5, left: 15},
        width = document.getElementById(grafico.substr(1)).parentElement.clientWidth - margin.left - margin.right,
        height = document.getElementById(grafico.substr(1)).parentElement.clientHeight - margin.top - margin.bottom - 25;

        //Modificación del formato de datos
        let newData = [];
        for(let i = 0; i < data.length; i++){
            if(data[i].pais == 'P. Bajos' || data[i].pais == 'Austria' || data[i].pais == 'Grecia' || data[i].pais == 'Alemania' || data[i].pais == 'Eurozona' ||
            data[i].pais == 'Portugal' || data[i].pais == 'Italia' || data[i].pais == 'España' || data[i].pais == 'Francia'){
                newData.push({categoria: data[i]['pais'], valores: [{descriptor:'I trim. 2021', valor: data[i].primer_trimestre}, 
                    {descriptor:'IV trim.', valor: data[i].cuarto_trimestre}, {descriptor:'III trim.', valor: data[i].tercer_trimestre} ]});
            }        
        }

        let paises = newData.map(function(d) { return d.categoria });
        let descriptores = newData[0].valores.map(function(d) { return d.descriptor});

        //Estructura del gráfico
        let chart = d3.select(grafico)
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje Y
        let y0 = d3.scaleBand()
            .rangeRound([height,0])
            .domain(paises)
            .padding(.5)
            .paddingOuter(.1);

        let y1 = d3.scaleBand()
            .range([y0.bandwidth(),0])
            .domain(descriptores);

        let yAxis = function(g){
            g.call(d3.axisRight(y0).tickSize(0))
            g.call(function(g){g.selectAll('.tick text')
                .style('fill', '#fff')
                .style('font-size','14.5px')
                .style('font-family','Roboto')
                .style('font-weight','300')
                .style('opacity','1')})
            g.call(function(g){g.select('.domain').style('opacity','0').attr('id', lineaEjeY)});
        }        

        chart.append("g")
            .attr('transform', 'translate(' + (width + 10) + ',0)')
            .call(yAxis);
        
        //Eje X
        let x = d3.scaleLinear()
            .domain([0.5,-12.5])
            .range([width,0.5]);

        let xAxis = function(g){
            g.call(d3.axisBottom(x).ticks(5))
            g.call(function(g){g.select('.domain').remove()})
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
            g.call(function(g){g.selectAll('.tick line')
                .attr("stroke-opacity", 0.5)
                .attr("stroke", "#343434")
                .attr("stroke-width", "1")
                .attr("y1", '0%')
                .attr("y2", '-' + (document.getElementById(lineaEjeY).getBoundingClientRect().height) + '')});
        }        

        chart.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);
        
        //Visualización de datos
        let slice = chart.selectAll(".slice")
        .data(newData)
        .enter().append("g")
        .attr("class", "g")
        .attr("transform",function(d) { return "translate(0," + y0(d.categoria) + ")"; });

        slice.selectAll("rect")
            .data(function(d) { return d.valores; })
            .enter()
            .append("rect")
            //.attr('width', function(d) { return x(0) - x(d.valor)})
            .attr("width", function(d) { return Math.abs(x(d.valor) - x(0)); })
            .attr('height', y1.bandwidth())
            .style('fill',  function(d){return d.descriptor == 'III trim.' ? '#661e1e' : d.descriptor == 'IV trim.' ? '#992d2d' : '#ff4a4a'})
            //.attr('x', function(d) { return x(d.valor)})
            .attr('x', function(d) { return d.valor > 0 ? x(0) : x(d.valor)})
            .attr('y', function(d) { return y1(d.descriptor)})
            .on('touchstart mouseover', function(d) {
                let bodyWidth = parseInt(d3.select('body').style('width'));
                let mapHeight = parseInt(d3.select('body').style('height'));
        
                // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
                // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
        
                tooltipExpectativas.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipExpectativas.html('<span style="font-weight: 900;">' + d.valor.toFixed(2).replace('.',',') + '</span>')
                    .style("left", ""+ (d3.event.pageX - 25) +"px")     
                    .style("top", ""+ (d3.event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipExpectativas.style('display','none').style('opacity','0');
            })
    } else {
        //Trabajar con homeExpectativas__data y homeExpectativas__arrow
        document.getElementById('homeExpectativas__data').textContent = data2[0].ultimo_trimestre.toFixed(1) + "%";
        //¿Es mejor o peor que el dato anterior?
        let prueba = data2[0].ultimo_trimestre < 0;
        prueba ? document.getElementById('homeExpectativas__arrow').innerHTML = '<img src="https://www.ecestaticos.com/file/754f36fc84032ae4e6444aa877433e19/1591887985-arrow-down.svg" width="32" height="12" alt="Flecha en descenso" title="Descenso respecto al dato anterior">' : document.getElementById('homeExpectativas__arrow').innerHTML = '<img src="https://www.ecestaticos.com/file/7e6a8ec1af77d3616135a0022edb227a/1591888407-arrow-up.svg" width="32" height="12" alt="Flecha en ascenso" title="Ascenso respecto al dato anterior">'; 
    }
    
}

function getDeficitCuentasPublicasHome(data2, data){
    if(window.innerWidth <= 767){
        let grafico = '#homeCuentasPublicas__data-mobile';
        let lineaEjeX = 'home-cuentas-deficit-ejeX';

        //Estructura fundamental del gráfico
        let margin = {top: 20, right: 15, bottom: 20, left: 20},
        width = document.getElementById(grafico.substr(1)).parentElement.clientWidth - margin.left - margin.right,
        height = document.getElementById(grafico.substr(1)).parentElement.clientHeight - margin.top - margin.bottom;
        
        //Inicialización del gráfico
        let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje X
        let x = d3.scaleBand()
            .range([0,width])
            .domain(d3.range(data.length))
            .paddingInner(.75)
            .paddingOuter(.25)
            .align(.5);

        let x2 = d3.scaleTime()
            .domain(d3.extent(data, function(d) {return formatMonthTime(d.mes)}))
            .range([0, width]);

        let xAxis = function(g){
            g.call(d3.axisBottom(x).tickFormat(function(d){if(d == 0){return '2004'} else if (d == data.length - 1){return '2020'}}))
            g.call(function(g){g.selectAll('.tick line').remove()})
            g.call(function(g){g.select('.domain').attr('id','domain-home')})
        }        

        chart.append('g')
            .attr('id', lineaEjeX)
            .attr('transform', 'translate(0, ' + height + ')')
            .call(xAxis);

        //Eje Y
        let y = d3.scaleLinear()
            .domain([-4,1])
            .range([height,0]);

        let yAxis = function(g){
            g.call(d3.axisLeft(y).ticks(6))
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d){ return d == 0 && 'tick-text-special'})})
            g.call(function(g){g.select('.domain').remove()})
            g.call(function(g){g.selectAll('.tick line')
                .attr("stroke-opacity", 0.5)
                .attr("stroke", "#343434")
                .attr("x1", '0%')
                .attr("x2", '' + (document.getElementById('domain-home').getBoundingClientRect().width) + '')});
        }        

        chart.append('g')
            .call(yAxis);

        //Visualización
        chart.selectAll('.barras')
            .data(data)
            .enter()
            .append('rect')
            .attr('fill', '#ff9574')
            .attr('y', function(d) { return y(Math.max(0,d.deficit)) })
            .attr('x', function(d,i) { return x(i)})
            .attr('height', function(d) {return Math.abs(y(d.deficit) - y(0));})
            .attr('width', x.bandwidth())
            .on('touchstart mouseover', function(d) {
                let bodyWidth = parseInt(d3.select('body').style('width'));
                let mapHeight = parseInt(d3.select('body').style('height'));

                // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
                // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;

                tooltipCuentas.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipCuentas.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes) + '</span><br><span style="font-weight: 900;">' + d.deficit.toFixed(2).replace('.',',') + '%</span>')
                    .style("left", ""+ (d3.event.pageX - 25) +"px")     
                    .style("top", ""+ (d3.event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipCuentas.style('display','none').style('opacity','0');
            })
    } else {
        //Trabajar con homeCuentasPublicas__data y homeCuentasPublicas__arrow
        document.getElementById('homeCuentasPublicas__data').textContent = data2[1].deficit.toFixed(1) + "%";
        //¿Es mejor o peor que el dato anterior?
        let prueba = data2[1].deficit < 0;
        prueba ? document.getElementById('homeCuentasPublicas__arrow').innerHTML = '<img src="https://www.ecestaticos.com/file/754f36fc84032ae4e6444aa877433e19/1591887985-arrow-down.svg" width="32" height="12" alt="Flecha en descenso" title="Descenso respecto al dato anterior">' : document.getElementById('homeCuentasPublicas__arrow').innerHTML = '<img src="https://www.ecestaticos.com/file/7e6a8ec1af77d3616135a0022edb227a/1591888407-arrow-up.svg" width="32" height="12" alt="Flecha en ascenso" title="Ascenso respecto al dato anterior">'; 
    }
}

function getAfiliadosEmpleoHome(data){
    let anchoPadre = document.getElementById('homeEmpleo__data').clientWidth;
    let anchoGraficos = (anchoPadre / 3) - 40;

    let margin = {top: 15, right: 10, bottom: 25, left: 30},
    width = anchoGraficos,
    height = 93;

    let provincias = d3.nest()
        .key(function(d) {return d.provincia;})
        .entries(data);

    let fechas = d3.nest()
        .key(function(d){ return d.fecha})
        .entries(data);

    let xScale = d3.scaleBand()
        .domain(fechas.map(function(d) {return d.key}))
        .align(1)
        .rangeRound([0, width]);

    let yScale = d3.scaleLinear()
        .domain([75,d3.max(data, function(d) { return d.dato + 15; })])
        .range([height, 10]);

    let xAxis = function(svg){
        svg.call(d3.axisBottom().scale(xScale).tickFormat(function(d){if(d == 'Febrero'){return 'feb.'}else if(d == 'Junio'){return 'jun.'}}))
        svg.call(function(g){g.selectAll('line').remove()}); 
    }         

    let yAxis = function(svg){
        svg.call(d3.axisLeft().scale(yScale).ticks(3))
        svg.call(function(g) {g.selectAll('.tick line').remove()});
    }        

    let line = d3.line()
        .x(function(d) { return xScale(d.fecha) + 10; })
        .y(function(d) { return yScale(d.dato); });

    let chart = d3.select("#homeEmpleo__data")
        .selectAll('prueba')
        .data(provincias)
        .enter()
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    chart.append("g").call(yAxis);
    chart.append("g").attr('transform', 'translate(0, ' + height + ')').call(xAxis);
    
    chart
        .append("text")
        .attr("text-anchor", "middle")
        .attr("y", 0)
        .attr("x", width/2)
        .text(function(d){
            let palabra = d.key;
            let possibleSplit = palabra.split(" ");
            let resultado = '';
            if(possibleSplit.length > 1){
                let primeraLetra = possibleSplit[0].charAt(0);
                let restoPrimera = possibleSplit[0].slice(1);
                let segundaLetra = possibleSplit[1].charAt(0);
                let restoSegunda = possibleSplit[1].slice(1);
                resultado = primeraLetra.toUpperCase() + restoPrimera.toLowerCase() + " " + segundaLetra.toUpperCase() + restoSegunda.toLowerCase();
            } else {
                let primeraLetra = palabra.charAt(0);
                let resto = palabra.slice(1);
                resultado = primeraLetra.toUpperCase() + resto.toLowerCase();
            }
            return resultado;
        })
        .style('fill', '#fff')
        .style('font-size','14.5px')
        .style('font-weight','300')
        .style('font-family','Roboto')
        .style('color','#fff')

    // // Add the line path elements
    chart.append("path")
        .attr("class", "line")
        .attr("stroke", "#ffe68c")
        .attr("stroke-width", "1px")
        .attr("d", function(d) { return line(d.values); });

    chart.selectAll("circle")
        .data(function(d){return d.values})
        .enter()
        .append("circle")
        .attr("r", 3)
        .attr("cx", function(d) { return xScale(d.fecha) + 10; })
        .attr("cy", function(d) { return yScale(d.dato); })
        .style("fill", "#ffe68c")
        .style("opacity", function(d,i) {if(i == 0 || i == provincias[0].values.length - 1){ return 1 } else { return 0 }})
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));

            let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            let downSide = mapHeight / 2 > d3.event.pageY ? false : true;

            tooltipEmpleo.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipEmpleo.html('<span style="font-weight: 300;">' + d.fecha + '</span><br><span style="font-weight: 900;">' + getNumberWithCommas(d.dato.toFixed(2).replace('.',',')) + '</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipEmpleo.style('display','none').style('opacity','0');
        })  
}

function getConsumoDemandaHome(data){
    //Trabajar con homeDemanda__data > Gráfico de fiebre
    let margin = {top: 5, right: 15, bottom: 20, left: 40},
    width = document.getElementById('homeDemanda__data').clientWidth - margin.left - margin.right,
    height = document.getElementById('homeDemanda__data').clientHeight - margin.top - margin.bottom;

    let svg = d3.select("#homeDemanda__data")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    let x = d3.scaleBand()
        .domain(data.map(function(d) { return d.mes_v2 }))
        .range([0, width])
        .padding(0.1);
    
    let xAxis = function(svg){
        svg.call(d3.axisBottom(x).tickFormat(function(d){ return d.substr(-4) }).tickValues(x.domain().filter(function(d,i) { return !(i%60) })))
        svg.call(function(g) {g.selectAll('.tick text').attr('text-anchor', function(d,i) { return i == 0 ? 'start' : i == 4 ? 'end' : 'middle'})})
        svg.call(function(g){g.selectAll('.tick line').remove()});
    }        
    
    svg.append('g')
        .attr('transform', 'translate(0, ' + height +')')
        .call(xAxis);

    //Eje Y
    let y = d3.scaleLinear()
        .domain([0, d3.max(data, function(d) {return d.indice_data+10})])
        .range([height,0]);

    let yAxis = function(svg){
        svg.attr("transform", 'translate(0,0)')
        svg.call(d3.axisLeft(y).ticks(6))
        svg.call(function(g){g.selectAll('.tick line').remove()});
    }        
    
    svg.append("g")
        .attr('transform', 'translate(0, 0)')
        .call(yAxis);

    //Line
    let line = d3.line()
        .defined(function(d){return !isNaN(d.indice_data)})
        .x(function(d){ return x(d.mes_v2)})
        .y(function(d){ return y(d.indice_data)});
    
    svg.append("path")
        .datum(data.filter(line.defined()))
        .attr("fill", "none")
        .attr("stroke", "#59fba5")
        .attr("stroke-width", 1)
        .attr('transform', 'translate(0, 0)')
        .attr("d", line);        

    //Círculos transparentes
    svg.selectAll('.circles')
        .data(data)
        .enter()
        .append('circle')
        .attr("r", 5)
        .attr("cx", function(d) { return x(d.mes_v2); })
        .attr("cy", function(d) { return y(d.indice_data); })
        .style("fill", '#ccc')
        .style('opacity', '0')
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipDemanda.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipDemanda.html('<span style="font-weight: 300;"> ' + formatTooltipDate(d.mes_v2) + '</span><br><span style="font-weight: 900;"> ' + d.indice_data.toFixed(2).replace('.',',') + '</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipDemanda.style('display','none').style('opacity','0');
        })
}

function getAfiliacionSectoresHome(data){    
    //Trabajar con homeSectores__data
    let margin = {top: 10, right: 17.5, bottom: 20, left: 100},
    width = document.getElementById('homeSectores__data').clientWidth - margin.left - margin.right,
    height = document.getElementById('homeSectores__data').clientHeight - margin.top - margin.bottom;
    
    // append the svg object to the body of the page
    let svg = d3.select("#homeSectores__data")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //X axis
    let x = d3.scaleLinear()
    .domain([-20, 10])
    .range([0, width]);

    let xAxis = function(svg){
        svg.call(d3.axisBottom(x).ticks(5))
        svg.call(function(g){g.selectAll('.domain').style('opacity','0').attr('id', 'homeAfiliacionSectores')})
        svg.call(function(g){g.selectAll('.tick line').remove()})
        svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        svg.attr("transform", "translate(0," + (height - 2.5) + ")");
    }        

    svg.append("g")
        .call(xAxis);

    // Y axis
    let y = d3.scaleBand()
    .range([0, height])
    .domain(data.map(function(d) { return d.sector_recortado; }))
    .paddingOuter(0);

    let yAxis = function(svg){
        svg.call(d3.axisLeft(y))
        svg.call(function(g){g.selectAll('.domain').remove()})
        svg.call(function(g){g.selectAll('.tick text').style('font-size','15px').style('opacity','1').style('font-weight','300')})
        svg.call(function(g){g.selectAll('.tick line')
            .attr('x2','0')
            .attr('x1','' + document.getElementById('homeAfiliacionSectores').getBoundingClientRect().width + '')
            .style('stroke','#343434')
            .style('stroke-dasharray','2')});
    }        

    svg.append("g")
    .call(yAxis);

    // Lines
    svg.selectAll("myline")
    .data(data)
    .enter()
    .append("line")
        .attr("x1", function(d) { return x(0); })
        .attr("x2", function(d) { return x(d.porc_afiliacion); })
        .attr("y1", function(d) { return y(d.sector_recortado) + (y.bandwidth() / 2); })
        .attr("y2", function(d) { return y(d.sector_recortado) + (y.bandwidth() / 2); })
        .attr("stroke", "lightgrey")
        .attr("stroke-width", "1px")

    // Circles of variable 1
    svg.selectAll("mycircle")
    .data(data)
    .enter()
    .append("circle")
        .attr("cx", function(d) { return x(0); })
        .attr("cy", function(d) { return y(d.sector_recortado) + (y.bandwidth() / 2); })
        .attr("r", "3")
        .style("fill", "#e4fbff")

    //Circles of 2
    svg.selectAll("mycircle")
    .data(data)
    .enter()
    .append("circle")
    .attr("cx", function(d) { return x(d.porc_afiliacion); })
    .attr("cy", function(d) { return y(d.sector_recortado) + (y.bandwidth() / 2); })
    .attr("r", "3")
    .style("fill", "#88ebff")
    .on('touchstart mouseover', function(d) {
        let bodyWidth = parseInt(d3.select('body').style('width'));
        let mapHeight = parseInt(d3.select('body').style('height'));

        // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
        // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;

        tooltipSectores.transition()     
            .duration(200)
            .style('display', 'block')
            .style('opacity','0.9')
        tooltipSectores.html('<span style="font-weight: 900;">' + getNumberWithCommas(d.porc_afiliacion.toFixed(2).replace('.',',')) + '</span>')
            .style("left", ""+ (d3.event.pageX - 25) +"px")     
            .style("top", ""+ (d3.event.pageY - 35) +"px");
    })
    .on('touchend mouseout mouseleave', function(d) {
        tooltipSectores.style('display','none').style('opacity','0');
    });
}

/* Funcionar para mostrar un bloque u otro en función de la interacción del usuario */
function showBlock(area) {    
    if(area == 'home'){
        document.getElementsByClassName('area')[0].classList.remove('active');
        document.getElementsByClassName('area')[0].classList.add('no-active');
        document.getElementsByClassName('slider')[0].classList.remove('active');
        document.getElementsByClassName('slider')[0].classList.add('no-active');
        document.getElementsByClassName('home')[0].classList.remove('no-active');
        document.getElementsByClassName('home')[0].classList.add('active');
        document.getElementsByClassName('menu')[0].classList.remove('menu--area');
    } else {
        document.getElementsByClassName('home')[0].classList.remove('active');
        document.getElementsByClassName('home')[0].classList.add('no-active');
        document.getElementsByClassName('slider')[0].classList.remove('active');
        document.getElementsByClassName('slider')[0].classList.add('no-active');
        document.getElementsByClassName('area')[0].classList.remove('no-active');
        document.getElementsByClassName('area')[0].classList.add('active');
        document.getElementsByClassName('menu')[0].classList.add('menu--area');
    }
}

//Dejamos pintado el tooltip genérico que utilizaremos para todos los gráficos
let tooltipProduccion = d3.select('body')
    .append('div')
    .attr('class', 'tooltip-dashboard tooltip-dashboard__produccion');

let tooltipExpectativas = d3.select('body')
    .append('div')
    .attr('class', 'tooltip-dashboard tooltip-dashboard__expectativas');

let tooltipCuentas = d3.select('body')
    .append('div')
    .attr('class', 'tooltip-dashboard tooltip-dashboard__cuentas');

let tooltipEmpleo = d3.select('body')
    .append('div')
    .attr('class', 'tooltip-dashboard tooltip-dashboard__empleo');

let tooltipDemanda = d3.select('body')
    .append('div')
    .attr('class', 'tooltip-dashboard tooltip-dashboard__demanda');

let tooltipSectores = d3.select('body')
    .append('div')
    .attr('class', 'tooltip-dashboard tooltip-dashboard__sectores');


//////
////// Funciones para carga de datos
//////

/* Función para recoger los datos desde Google SpreadSheets */
function getData(data, type) {    
    let http = new XMLHttpRequest();
    http.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let response = http.response;
            if(response == ""){
                console.error('No hay nada. Error en la llamada');
            } else {
                let formatData = type == 'dataHome' || type == 'commonElements' ? csvToJson(response, 'home') : csvToJson(response);
                initData(formatData, type);
                //Aquí podríamos llevar a display 'none' a home__loading y a 'block/flex' a home__cards
                type == 'dataHome' && createHome();
            }   
        }
    }
    //data = data.replace('content.google','www.google');
    http.open('get', data, false);
    http.send();
}

/* Función para parsear datos de TSV a objetos JavaScript --> Algo tendremos que hacer para titulares, subtítulos y fuentes */
function csvToJson(csv, dataHome){
    let lines=csv.split("\n");    
    let result = [];

    let headers = dataHome ? lines[1].split("\t") : lines[3].split("\t");
    let inicio = dataHome ? 2 : 4;

    for(i = 0; i < headers.length; i++){
        headers[i] = headers[i].trim();
    }

    for(let i=inicio;i<lines.length;i++){ //Datos respectivos a headers, a partir de quinta línea
        let obj = {};
        let currentline=lines[i].split("\t");

        for(let j=0;j<headers.length;j++){
            if(headers[j] != ''){ //26/05 --> Queda una cabecera vacía. Lo dejamos así de momento
                obj[headers[j]] = currentline[j];
            }            
        }
        result.push(obj);
    }
    return result;
}

/* Recogemos los datos para integrarlos en los diferentes indicadores */
function initData(data, typeOfData){    
    if(typeOfData == 'dataHome'){
        for(let i = 0; i < data.length; i++){
            data[i].Trimestre_PIB_Generico != '' && pibTrimestralProduccion.push({trimestre: data[i].Trimestre_PIB, trimestre_v2: data[i].Trimestre_PIB_Generico, pib_data: +data[i].PIB.replace(',','.')});
            data[i].ID_PAIS != '' && pibEurozonaExpectativas.push({id_pais: data[i].ID_PAIS, pais: data[i].PAIS, trimestre_anterior: +data[i]['III Trimestre'].replace(',','.'), ultimo_trimestre: +data[i]['IV Trimestre'].replace(',','.')});
            data[i].Mes_Deficit_Generico != '' && deficitEstadoCuentasPublicas.push({mes: data[i].Mes_Deficit, mes_v2: data[i].Mes_Deficit_Generico, deficit: +data[i].Deficit_Dato.replace(',','.')});
            data[i].Provincia_Evolucion != '' && numeroAfiliadosEmpleo.push({fecha: data[i].Fecha_Evolucion, provincia: data[i].Provincia_Evolucion, dato: +data[i].Dato.replace(',','.')})
            data[i].Mes_Historico_Generico != '' && evolucionConsumoDemanda.push({mes: data[i].Mes_Historico, mes_v2: data[i].Mes_Historico_Generico, indice_data: +data[i].Indice.replace(',','.')});
            data[i].Sector_Afiliacion != '' && variacionAfiliacionSectores.push({sector: data[i].Sector_Afiliacion, sector_recortado: data[i].Sector_Af_Recortado, porc_afiliacion: +data[i].Porcentaje_Afiliacion.replace(',','.')});
        }
    } else if (typeOfData == 'dataProduccion') {
        for(let i = 0; i < data.length; i++){
            data[i].PIB != '' && produccionPib.push({trimestre: data[i].Trimestre_PIB, trimestre_v2: data[i].Trimestre_PIB_Generico, pib_data: +data[i].PIB.replace(',','.')});            
            data[i]['PAIS'] != '' && produccionPibEurozona.push({pais: data[i]['PAIS'], dato_trimestre: +data[i]['Primer trimestre'].replace(',','.'), dato_trimestre_2: +data[i]['Segundo trimestre'].replace(',','.')});
            data[i].IP != '' && produccionInversionProductiva.push({trimestre: data[i].Trimestre_IP, trimestre_v2: data[i].Trimestre_IP_Generico, ip_data: +data[i].IP.replace(',','.')});
            data[i].ID_PROVINCIA != '' && produccionEmpresasActivas.push({id_provincia: +data[i].ID_PROVINCIA, provincia: data[i].Provincia, cambio_data: +data[i].Cambio_desde_inicio_crisis.replace(',','.')});
            data[i].PI != '' && produccionProdIndustrial.push({id_ccaa: +data[i].ID_CCAA, ccaa: data[i].CCAA, pi_data: +data[i].PI.replace(',','.')});
            data[i].Trimestre_Correlacion != '' && produccionPibEmpleo.push({trimestre: data[i].Trimestre_Correlacion, pib_correlacion_data: +data[i].PIB_Correlacion.replace(',','.'), horas_efectivas_data: +data[i].Horas_efectivas.replace(',','.')});
        }
    } else if (typeOfData == 'dataEmpleo') {
        for(let i = 0; i < data.length; i++){
            data[i].ID_Provincia_Evolucion != '' && empleoAfiliacionProvincias.push({provincia: data[i].ID_Provincia_Evolucion, Febrero: +data[i].Febrero.replace(',','.'), Marzo: +data[i].Marzo.replace(',','.'), Abril: +data[i].Abril.replace(',','.'), Mayo: +data[i].Mayo.replace(',','.'), Junio: +data[i].Junio.replace(',','.')})
            data[i].Periodo_Afiliacion != '' && empleoAfiliacionHistorico.push({fecha: data[i].Periodo_Afiliacion, afiliados: +data[i].Afiliados});
            data[i].Trimestre_Paro_Generico != '' && empleoTasaParoHistorica.push({trimestre: data[i].Trimestre_Paro, trimestre_v2: data[i].Trimestre_Paro_Generico, tasa_paro: +data[i].Tasa_Paro.replace(',','.')});
            data[i].Pais_Numero_Empleados != '' && empleoCaidaEmpleo.push({pais: data[i].Pais_Numero_Empleados, variacion_empleados: +data[i].Variacion_Empleados.replace(',','.')});
            data[i].Pais_Paro != '' && empleoTasaParoEuropa.push({pais: data[i].Pais_Paro, datos_paro: +data[i].Dato_Paro.replace(',','.')});
            data[i].Mes_Paro != '' && empleoNumeroParados.push({mes: data[i].Mes_Paro, mes_v2: data[i].Mes_Paro_Generico, paro_registrado: +data[i].Paro_Registrado});
        }
    } else if (typeOfData == 'dataDemanda') {
        for(let i = 0; i < data.length; i++){
            data[i].Mes_Historico != '' && demandaMinoristaHistorica.push({mes: data[i].Mes_Historico, mes_v2: data[i].Mes_Historico_Generico, indice_precios_corrientes: +data[i]['Índice'].replace(',','.')});
            data[i].Sector != '' && demandaMinoristaSectorial.push({sector: data[i].Sector, sector_mostrar: data[i].Sector_Mostrar, dato_enero: +data[i]['enero/2020'].replace(',','.'), dato_febrero: +data[i]['febrero/2020'].replace(',','.'), dato_marzo: +data[i]['marzo/2020'].replace(',','.'), dato_abril: +data[i]['abril/2020'].replace(',','.'), dato_mayo: +data[i]['mayo/2020'].replace(',','.'), dato_junio: +data[i]['junio/2020'].replace(',','.')});
            data[i].ID_CCAA != '' && demandaMinoristaCCAA.push({id_ccaa: data[i].ID_CCAA, ccaa: data[i].CCAA, variacion_mensual: +data[i]['Variación mensual'].replace(',','.'), variacion_anual: +data[i]['Variación anual'].replace(',','.')});
            data[i].ID_PAIS != '' && demandaMinoristaEurozona.push({id_pais: data[i].ID_PAIS, pais: data[i].PAIS, consumo: +data[i]['Consumo'].replace(',','.')});
        }
    } else if (typeOfData == 'dataExpectativas') {
        for(let i = 0; i < data.length; i++){
            data[i].ID_PAIS != '' && expectativasPrevisiones.push({id_pais: data[i].ID_PAIS, pais: data[i].PAIS, tercer_trimestre: +data[i]['III Trimestre'].replace(',','.'), cuarto_trimestre: +data[i]['IV Trimestre'].replace(',','.'), primer_trimestre: +data[i]['I Trimestre 2021'].replace(',','.')}); //Cambiar
            data[i].PAIS_CONFIANZA != '' && expectativasPMI.push({pais: data[i].PAIS_CONFIANZA, confianza: +data[i].Porcentaje_Confianza.replace(',','.')});
            data[i].Mes_Confianza != '' && expectativasConfianzaConsumidor.push({mes: data[i].Mes_Confianza, mes_v2: data[i].Mes_Confianza_Generico, manufacturas: +data[i]['Manufacturas'].replace(',','.'), servicios: +data[i]['Servicios'].replace(',','.'), total_economia: +data[i]['Total economía'].replace(',','.')});
            data[i].Mes_Evolucion_Eco != '' && expectativasHogaresEconomia.push({mes: data[i]['Mes_Evolucion_Economica'], mes_v2: data[i]['Mes_Evolucion_Eco_Generico'] ,situacion_economica: +data[i]['General economic situation over the next 12 months'].replace(',','.')});
            data[i].Mes_Evolucion_Paro != '' && expectativasHogaresParo.push({mes: data[i]['Mes_Evolucion_Paro'], mes_v2: data[i]['Mes_Evolucion_Paro_Generico'], situacion_empleo: +data[i]['Unemployment expectations over the next 12 months'].replace(',','.')});
        }
    } else if (typeOfData == 'dataSectores') {
        for(let i = 0; i < data.length; i++){
            data[i].Sector_Afiliacion != '' && sectoresCambioAfiliacion.push({sector: data[i].Sector_Afiliacion, sector_mostrar: data[i].Sector_Af_Mostrar, data_porcentaje: +data[i].Porcentaje_Afiliacion.replace(',','.'), data_absoluto: +data[i].Absoluto_Afiliacion.replace(',','.')});
            data[i].Sector_VA != '' && sectoresValorAnadido.push({sector: data[i].Sector_VA, sector_mostrar: data[i].Sector_VA_Mostrar, variacion: +data[i].Variacion_VA.replace(',','.')});
            data[i].Provincia_ID != '' && sectoresLlegadaViajeros.push({provincia_id: data[i].Provincia_ID, provincia: data[i].Provincia_Nombre, turistas_total: +data[i]['Total'].replace(',','.'), turistas_espana: +data[i]['Residentes en España'].replace(',','.'), turistas_extranjero: +data[i]['Residentes en el extranjero'].replace(',','.')});
        }
    } else if (typeOfData == 'dataCuentasPublicas') {
        for(let i = 0; i < data.length; i++){
            data[i].Mes_Deficit_Generico != '' && cuentasDeficitHistorico.push({mes: data[i].Mes_Deficit_Generico, deficit: +data[i].Deficit_Dato.replace(',','.')});
            data[i]['Figura tributaria'] != '' && cuentasRecaudacion.push({figura_tributaria: data[i]['Figura tributaria'], figura_mostrar: data[i].Figura_Mostrar, variacion_porc: +data[i]['Variación en porcentaje'].replace(',','.'), variacion_millones: +data[i]['Variación en millones de euros'].replace(',','.'), variacion_acumulada_porc: +data[i]['Variación acumulada en lo que va de año en porcentaje'].replace(',','.')});
            data[i].Trimestre_Deuda_Generico != '' && cuentasDeudaPublica.push({trimestre: data[i].Trimestre_Deuda_Generico, porcentaje_deuda: +data[i].Porcentaje_Deuda.replace(',','.')});
            data[i].ID_CCAA != '' && cuentasDeficitCCAA.push({id_ccaa: data[i].ID_CCAA, ccaa: data[i].CCAA, deficit_acumulado: +data[i].Deficit_Acumulado.replace(',','.')});
        }
    } else if (typeOfData == 'commonElements'){
        for(let i = 0; i < data.length; i++){
            commonElementsMap.set(data[i].ID_Grafico, {id_grafico: data[i].ID_Grafico, area: data[i].Area_Interes, area_desarrollo: data[i].Area_Interes_Desarrollo, titulo_area_home: data[i].Titulo_Portada_Area, titulo_interior: data[i].Titulo, subtitulo_interior: data[i].Subtitulo, fuente_interior: data[i].Fuente, ultima_actualizacion: data[i].Ultima_Actualizacion, descriptor_ejeX: data[i].Descriptor_EjeX_Opcional, descriptor_ejeY: data[i].Descriptor_EjeY_Opcional});
        }
        getLastUpdate('home');
    }    
}

//////
////// Implementación de lógica para carrusel móvil de las cards en la Home
//////
let homeCards = document.getElementById("homeCards");
let activeCard = 1;
let viewportWidth = window.innerWidth;
let arrowNext = document.getElementById("nextCard");
let arrowPrev = document.getElementById("prevCard");

arrowNext.addEventListener('click', function(e) {
    e.preventDefault();
    activeCard == 6 ? activeCard = 1 : activeCard++;
    updateCard(activeCard);
    getOutTooltips();
});
arrowPrev.addEventListener('click', function(e) {
    e.preventDefault();
    activeCard == 1 ? activeCard = 6 : activeCard--;
    updateCard(activeCard);
    getOutTooltips();
});

if ( isMobile() ) {
    homeCards.addEventListener('swiped-left', function(evt) {
        activeCard == 6 ? activeCard = 1 : activeCard++;
        updateCard(activeCard);
    });
    homeCards.addEventListener('swiped-right', function(evt) {
        activeCard == 1 ? activeCard = 6 : activeCard--;
        updateCard(activeCard);
    });
}

function updateCard(index) {
    console.log(viewportWidth);
    console.log(window.innerWidth);
    let translate = viewportWidth * (index - 1);
    homeCards.style.transform = "translateX(-"+translate+"px)";
    updateDots(index);
}

function updateDots(index) {
    let cardDots = document.getElementsByClassName('card__dot');
    let currentDotActive = document.getElementsByClassName('card__dot--active');
    currentDotActive[0].classList.remove("card__dot--active");
    cardDots[index-1].classList.add("card__dot--active");
}

function isResponsive() {
    return window.innerWidth < 993;
}

function isMobile() {
    return window.innerWidth < 768;
}

function formatTimeData(data){
    let result = [];
    for(let i = 0; i < data.length; i++){
        let item = data[i];
        let provincia = '';
        if (!Object.entries) {
            Object.entries = function( obj ){
              var ownProps = Object.keys( obj ),
                  i = ownProps.length,
                  resArray = new Array(i); // preallocate the Array
              while (i--)
                  resArray[i] = [ownProps[i], obj[ownProps[i]]];
          
              return resArray;
            };
        } else {
            Object.entries(item).forEach(function(item2, index) {
                if(index == 0){
                    provincia = item2[1];
                } else {
                    result.push({fecha: item2[0], provincia: provincia, dato: item2[1]});
                }            
            });
        }        
    }
    return result;
}

function scaleBandPosition(mouse, x) {
    var xPos = mouse[0];
    var domain = x.domain(); 
    var range = x.range();
    var rangePoints = d3.range(range[0], range[1], x.step());
    let prueba = d3.bisectLeft(rangePoints, xPos);
    return domain[prueba];
}

function formatTime(date) {
    let split = date.split("/");
    return new Date(split[1], split[0] - 1, 1);
}

function formatTime(date, especifico) {
    let split = date.split("/");
    let number = 0;
    let year = split[1];
    if(especifico == 'cuentas_publicas'){
        for(let i = 0; i < mesesReducido.length; i++){
            if(split[0].substr(0,3).toLowerCase() == mesesReducido[i].toLowerCase()){
                number = i;
            }
        }
        year = year < 25 ? '20' + year : '19' + year; //Feo > Peor hay que adecuarse a lo dispuesto por Javier
    } else {
        for(let i = 0; i < meses.length; i++){
            if(split[0].toLowerCase() == meses[i].toLowerCase()){
                number = i;
            }
        }
    }
    
    return new Date(year, number, 1);
}

function formatMonthTime(date){
    let split = date.split("/");
    return new Date(split[1], 4 - 1, 1);
}

function formatDayTime(date){
    let split = date.split("/");
    return new Date(split[2], split[1] - 1, split[0]);
}

function formatYear(date) {
    let prueba = date.getFullYear();
    return prueba;
}

function getNumberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function formatTooltipDate(date){
    //Puede venir 'febrero/2007' o '7/2007' o '7/07/2020'
    let split = date.split("/");
    let dia = '', mes = '', anio = '';

    let resultado = '';

    if(isNaN(split[0])){
        mes = split[0];
        let letra = mes.charAt(0);
        letra = letra.toUpperCase();
        mes = letra + mes.slice(1);
        anio = split[1];

        resultado = mes + ' ' + anio;

    } else {
        let indice;
        if(split.length > 2){
            indice = parseInt(split[1]) - 1;
            anio = split[2];
            mes = meses[indice].toLowerCase();
            dia = split[0];

            resultado = dia + ' de ' + mes + ' ' + anio;
        } else {
            indice = parseInt(split[0]) - 1;
            anio = split[1];
            mes = meses[indice];

            resultado = mes + ' ' + anio;
        }      
        
    }
    return resultado;
}

//Línea de regresión
function calculateRegression(XaxisData, Yaxisdata) {
    let ReduceAddition = function(prev, cur) { return prev + cur; };
    
    // finding the mean of Xaxis and Yaxis data
    let xBar = XaxisData.reduce(ReduceAddition) * 1.0 / XaxisData.length;
    let yBar = Yaxisdata.reduce(ReduceAddition) * 1.0 / Yaxisdata.length;

    let SquareXX = XaxisData.map(function(d) { return Math.pow(d - xBar, 2); })
      .reduce(ReduceAddition);
    
    let ssYY = Yaxisdata.map(function(d) { return Math.pow(d - yBar, 2); })
      .reduce(ReduceAddition);
      
    let MeanDiffXY = XaxisData.map(function(d, i) { return (d - xBar) * (Yaxisdata[i] - yBar); })
      .reduce(ReduceAddition);
      
    let slope = MeanDiffXY / SquareXX;
    let intercept = yBar - (xBar * slope);
    
    // returning regression function
    return function(x){
      return x*slope+intercept;
    }
}

//Invisibilidad de tooltips
function getOutTooltips(){
    let tooltips = document.getElementsByClassName('tooltip-dashboard');
    for(let i = 0; i < tooltips.length; i++){
        tooltips[i].style.display = 'none';
        tooltips[i].style.opacity = '0';
    }
}

//////
////// Función para conocer la última actualización en home y portadas de sección
//////
function getLastUpdate(seccion){
    //Recorrer el mapa
    let lastDate = new Date(2000,1,1); //Fecha cualquiera (menor a 2020) de iniciación de comparativa   

    if(seccion == 'home'){
        commonElementsMap.forEach(function(item) {
            let split = item.ultima_actualizacion.split("/");
            let fecha = new Date(split[2], split[1] - 1, split[0]);            
            if(lastDate.getTime() - fecha.getTime() < 0){
                lastDate = fecha;
            }    
        });    
        document.getElementById('lastUpdateHome').textContent = 'Datos actualizados a ' +lastDate.getDate()+ ' de ' +meses[lastDate.getMonth()].toLowerCase()+ ' de ' +lastDate.getFullYear();
    } else {
        let auxiliarMap = new Map(commonElementsMap);
        auxiliarMap.forEach(function(item,key) {
            if(item.area_desarrollo != seccion){
                auxiliarMap.delete(key)
            }
        });
        //Tras el filtrado por sección, la comparativa para quedarnos con la última fecha
        auxiliarMap.forEach(function(item) {
            let split = item.ultima_actualizacion.split("/");
            let fecha = new Date(split[2], split[1] - 1, split[0]);
            
            if(lastDate.getTime() - fecha.getTime() < 0){
                lastDate = fecha;
            }    
        });
        document.getElementById('lastUpdateArea').textContent = 'Datos actualizados a ' +lastDate.getDate()+ ' de ' +meses[lastDate.getMonth()].toLowerCase() + ' de ' +lastDate.getFullYear();
    }    
}